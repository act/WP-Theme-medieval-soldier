<?php
 
function get_rank_list($conn=null)
	{
		if($conn)
			{			
				$queryall="
						SELECT 
							`r_Rank`.`Presentation` AS `rank`
						FROM
							`r_Rank`
						WHERE
							(`r_Rank`.`Is_Standard_Value` = 1);
				"; 
				
				//echo $queryall;
					try
						{	
							$data=$conn->query($queryall);
							//$conn->close();
							if($data!=null)
								{	return $data;}
						}
						
						
					catch(Exception $e)
						{
							//echo $e;
							return false;
							//$conn->close();
						}
			}

		return false;	
	}
	

/*
//test
include "../dbopen.php";
$d=get_rank_list($conn);

if($d)
	{
		if ($d->num_rows > 0) {
			echo "<table><tr><th>ID</th><th>Name</th></tr>";
			// output data of each row
			while($row = $d->fetch_assoc()) {
				echo "<tr><td>".$row["rank"]."</td></tr>";
			}
			echo "</table>";
		} else {
			echo "0 results";
		}
	}
else{echo "no results!---".$d;}
*/


?>