<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage MedievalSoldier
 * @since Twenty Thirteen 1.0
 */

$menu_obj = get_menu_by_location("primary"); 

//$menu = wp_get_menu_array('auto-menu-from-pages');
$menu = wp_get_menu_array($menu_obj->name);
get_header();
?>


<body>
	<a id="skippy" class="sr-only sr-only-focusable" href="#content">
		<div class="container"><span class="skiplink-text">Skip to main content</span></div>
	</a>

	<div class="dropdown navbar-dropdown navbar-dropdown-menu">
			<div class="navbar-fixed navbar-fixed-top">
				<nav class="navbar-inverse navbar" style="background-color:black">
					<div class="container navbar-container">
						<div class="container col-sm-6" style="background-color:black;padding:0px">
							<div class="container col-sm-12" style="background-color:black;padding:0px" >
								<a class="navbar-brand navbar-brand-hbs" href="http://www.henley.ac.uk/">Henley Business School</a>

								<a class="navbar-brand navbar-brand-soton" href="http://www.southampton.ac.uk"> Soton</a>
							</div>
						</div>


							<!-- ########################################################### MOBILE NAVIGATION TOGGLE ########################################################### -->
						<div class="dropdown navbar-dropdown navbar-dropdown-menu">			                
							<button type="button" class="navbar-toggle" data-toggle-group="main-nav" id="navbar-toggle-mobile" data-target="#mobile-nav" data-toggle-mobilenav>
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar" aria-hidden="true"></span>
								<span class="icon-bar" aria-hidden="true"></span>
								<span class="icon-bar" aria-hidden="true"></span>
								<span class="navbar-toggle-close">&times;</span>
							</button>
							<!-- Mobile navigation -->
							<nav class="navbar-nav-mobile-wrapper island-gray-darker" id="mobile-nav" aria-labelledby="navbar-toggle">
								<ul class="nav nav-arrow-right navbar-nav navbar-nav-mobile nav-dark">
										<!-- active -->
										<!-- TOP LEVEL MENU LINKS -->
										<?php 
										// loop on the menu items to build the first menu level
										$i=1;
										foreach ($menu as $key => $menu_item){
											echo "<li class=\"dropdown\">";
											if ($i==1) echo "<a href=\"/\">Home</a>";
											if (strpos($menu_item['url'], '#') !== false) { //if the URL is just a link to a sub panel, then we add some extra code for the javascript to open it
												echo "<a href=\"#\" data-toggle=\"dropdown\" data-target=\"#\" id=\"subnav".$i."-toggle\">".$menu_item['title']."</a>";
												//level2 menu:
												echo "<ul class=\"nav nav-sub nav-light nav-arrow-right\" id=\"subnav-".$i."\" aria-labelledby=\"subnav".$i."-toggle\">";
												if ($menu_item['children']) { //second level menu
													echo "<li class=\"nav-item-dark nav-item-arrow-light nav-item-arrow-left\"><a href=\"#subnav-".$i."\" data-toggle=\"dropdown\" data-target=\"#subnav-".$i."\">Back</a></li>";
													echo "<li class=\"dropdown\">";
														foreach ($menu_item['children'] as $key => $submenu_item){
															echo "<li><a href=\"".$submenu_item['url']."\" data-close-desktopnav>".$submenu_item['title']."</a></li>";
														}
													echo "</li>";
												}
												echo "</ul>";
												$i++;
											}
											else {
												echo "<a href=\"".$menu_item['url']."\">".$menu_item['title']."</a>";
											}
											echo "</li>";
										}
										
										?>	
								</ul>
								
							</nav>
						</div> 
						 <!-- END MOBILE NAVIGATION -->
						
						
						<!-- ########################################################### DESKTOP NAVIGATION ########################################################### -->
						<nav class="navbar-right navbar-nav-desktop" style="background-color:#000000;">
							<div class="cell-v5 clearfix">
								<div class="btn-group btn-group-bar pull-right text-muted" role="group">
									<a href="https://www.reading.ac.uk" class="btn btn-plain text-small ">University of Reading</a>
									<!-- <a href="#" class="btn btn-plain text-small ">Alumni</a> -->
									<!-- <a href="#" class="btn btn-plain text-small ">International</a> -->
									<a href="http://www.southampton.ac.uk" class="btn btn-plain text-small">University of Southampton</a>
								</div>
							</div>
							<ul class="nav navbar-nav" id="desktop-nav">
								<li>
									<!--  class="active" -->
									<a href="<?php echo get_home_url(); ?>" >
									<img class="" src="<?php echo get_template_directory_uri(); ?>/images/home.png" style="width:1em; height:1em;">
									</a>
								</li>
								<?php 
								// loop on the menu items to build the first menu row
								foreach ($menu as $key => $menu_item){
									if (strpos($menu_item['url'], '#') !== false) { //if the URL is just a link to a sub panel, then we add some extra code for the javascript to open it
									echo "<li><a href=\"".$menu_item['url']."\"  data-toggle-desktopnav data-target=\"".$menu_item['url']."\" data-toggle-group=\"main-nav\">".$menu_item['title']."</a>";
									}
									else {
										echo "<li><a href=\"".$menu_item['url']."\">".$menu_item['title']."</a>";
									}
									echo "</li>";
								}
								?>  								
							</ul>
						</nav>
					</div>
					<!-- /.container-fluid -->
				</nav>
				<!-- Desktop sub navigation -->
				<?php
				// loop on the menu items to build the second menu panel
				$i=1;
				foreach ($menu as $key => $menu_item){
					if ($menu_item['children']) {
						echo "<ul class=\"nav nav-sub nav-sub-desktop\" id=\"subnav-desk-".$i."\">";
							echo "<li class=\"dropdown\">";
								echo "<a href=\"#subnav-desk-".$i."-subnav-desk-1\" data-toggle=\"toggle\" data-toggle-group=\"subnav-desk-".$i."\" data-target=\"#subnav-desk-".$i."-subnav-1\" data-toggle-min=\"1\" class=\"active\">".$menu_item['title']."</a>";
								echo "<ul id=\"subnav-desk-".$i."-subnav-1\" class=\"nav nav-sub open\">";
								foreach ($menu_item['children'] as $key => $submenu_item){
									echo "<li><a href=\"".$submenu_item['url']."\" data-close-desktopnav>".$submenu_item['title']."</a></li>";
								}
						echo "</ul></li></ul>";
						$i++;
					}
				}
				?>	
				
			</div>
				  
	</div><!-- /.container-fluid -->
						 <!-- END DESKTOP NAVIGATION -->
						 
						 
	<main role="main" id="content">
		<!-- hero -->
	<div class="cell-v30 jumbotron island island-gray-dark jumbotron-img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/background.jpg')">
		<div class="container">
			<div class="row">
		<div class="col-xs-6">
			<h2 class="block-heading block-heading-l5 block-heading-b5 block-heading-md-l-reset cell-md-t20">
				<b>The Soldier in Later Medieval England</b>
			</h2>
		</div><!--//.col-->
	</div><!--//.row-->

		</div>
		<!--//.container-->
	</div>


	<div class="container-fluid" style="padding-top: 50px;">
				
			<!-- ############## start intro content ############## -->
			<div class="container col-sm-12">
				<div class="col-xs-12 col-md-8">
					<h2>Not Found</h2>
					<p>The page you are looking for has been moved, or is no longer accessible. Please navigate in the above menu.</p>
				</div>
				<!--//.col-->

				<!--//.col-->
			</div>
		<!--//.row-->
		<!-- ############## end intro content ############## -->
	</div>
<!--//.contain -->


<?php get_footer(); ?>
