    
function showhint(id,txt){
    tt = document.getElementById('tooltip'); 
    tt.innerHTML=txt;
	tooltip_show('tooltip',id,5,0);
  }  
    
function tooltip_hide(id){
    it = document.getElementById(id); 
    it.style.visibility = 'hidden'; 
  }
  
function tooltip_show(tooltipId, parentId, offsetX, offsetY){

  if(!offsetX){offsetX=0;}
  if(!offsetY){offsetY=0;}
  
    tt = document.getElementById(tooltipId);
    obj = document.getElementById(parentId); 
            
     // need to fixate default size (MSIE problem)
    tt.style.width = tt.offsetWidth + 'px';
    tt.style.height = tt.offsetHeight + 'px';
    
                x = findPosX(obj);  
                y = findPosY(obj);  
                
    if(x==0 && y==0){  // not containerised
     anchorX = obj.offsetLeft+obj.offsetWidth + offsetX ;
     anchorY = obj.offsetTop+obj.offsetHeight + offsetY ;
    }
    else{  // containerised
      anchorX = obj.offsetWidth + x + offsetX ;
      anchorY = obj.offsetHeight + y + offsetY ;
     }
     divw= parseInt(tt.style.width.split("p")[0]);  // 
     divh= parseInt(tt.style.height.split("p")[0]) ;   // 
     xmax=divw + anchorX;
     ymax=divh + anchorY;
     if(xmax>window.innerWidth){
       anchorX=anchorX-divw;
     }
     if(ymax>window.innerHeight){
       anchorY=anchorY-divh-obj.offsetHeight;
     }
    tt.style.left = anchorX + 'px';
    tt.style.top = anchorY + 'px';
    tt.style.visibility = 'visible';

    //alert("divw+anchorX="+xmax + "\r\n window.innerWidth="+window.innerWidth);     
    //alert("div w="+divw + "\r\n divh="+divh);
    //alert("doc-innerwidth="+window.innerWidth+"\r\n odc-window.innerHeight= "+window.innerHeight);
  //   alert("  obj.Height="+obj.Height+"\r\n  obj.Width="+obj.Width+"\r\nobj.offsetLeft="+obj.offsetLeft+"\r\n  obj.offsetTop="+obj.offsetTop+"\r\n  obj.offsetWidth="+obj.offsetWidth+"\r\n  obj.offsetHeight="+obj.offsetHeight+"\r\n x="+x+"\r\n  y="+y);
   }
  

function findPosX(obj) {
  	var curleft = 0;
  	if (obj.offsetParent !=document.body) {
  		curleft = obj.offsetLeft
  		while (obj = obj.offsetParent) {
  			curleft += obj.offsetLeft
  			}
  	}
  	return curleft;
  }

  
function findPosY(obj) {
  	var curtop = 0;
  	if (obj.offsetParent !=document.body) {
  		curtop = obj.offsetTop
  		while (obj = obj.offsetParent) {
  			curtop += obj.offsetTop
  			}
  	}
  	return curtop;
  }


  
  
function countdown(el,limit)
{
	var cddiv=document.getElementById('countdowndiv');
	var clen=el.value.length;
	var rmg=limit-clen; 
	if(clen>limit){el.value=el.value.substring(0,limit);}
	clen=el.value.length;
	rmg=limit-clen; 
	cddiv.innerHTML='<font size="-1" color="#00cdef">' +String(rmg)+' characters remaining</font>';
	cddiv.style.visibility='visible';
	tooltip_show(cddiv.id,el.id,15,20);
}

function hidecountdown()
{
	var cddiv=document.getElementById('countdowndiv');	
	cddiv.style.visibility='hidden';
}	

function scrollToPos(posx,posy) {
  if(!posx){posx=0;}
  if(!posy){posy=0;}
  window.scrollTo(posx,posy);
  
}
function saveScrollPos(frmobj) {//alert(frmobj.name);
  frmobj.scrollx.value = (document.all)?document.body.scrollLeft:window.pageXOffset;
  frmobj.scrolly.value = (document.all)?document.body.scrollTop:window.pageYOffset;
}

function showmessage(str){alert(str);}

function SubmitMe(fname){f=document.forms[fname];
	saveScrollPos(f);
	f.submit();
	return true;}


function IsDate(dateStr)
	// This function accepts a string variable and verifies if it is a
	// proper date or not. It validates format matching either
	// mm-dd-yyyy or mm/dd/yyyy. Then it checks to make sure the month
	// has the proper number of days, based on which month it is.

	// The function returns true if a valid date, false if not.
	// ******************************************************************
	{
		//Modified by DO 12/31/2003
		var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
		var matchArray = dateStr.match(datePat); // is the format ok?
		var datestatus=true;
		datemsg="";

		if (matchArray == null || matchArray[1]==null)
			{
				datemsg="----- Please enter date as mm/dd/yyyy " + "\n";
				alert(datemsg);
				return false;
			}
		else
			{
				if(matchArray[3]=null || matchArray[5]==null)
					{
						datemsg="----- Please enter date as mm/dd/yyyy " + "\n";
						alert(datemsg);
						return false;
					}
			}
			
			month = matchArray[1]; // p@rse date into variables
			day = matchArray[3];
			year = matchArray[5];

			if (month < 1 || month > 12)
			{ // check month range
				datemsg=datemsg + "----- Month must be between 1 and 12." + "\n";
				datestatus=false;
			}

			if (day < 1 || day > 31)
			{
				datemsg=datemsg + "----- Day must be between 1 and 31." + "\n";
				datestatus=false;
			}

			if ((month==4 || month==6 || month==9 || month==11) && day==31)
			{
				datemsg=datemsg + "----- Month " + month + " doesn`t have 31 days!" + "\n";
				datestatus=false;
			}

			if (month == 2)
			{ // check for february 29th
				var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
				if (day > 29 || (day==29 && !isleap))
					{
						datemsg=datemsg + "----- February " + year + " doesn`t have " + day + " days!" + "\n";
						datestatus=false;
					}
			}
		return datestatus;
	}
