<?php



$dbhost = 'medevil-soldier.cluster-cbyhtlqnf3fm.us-east-1.rds.amazonaws.com';
$dbuser = 'root';
$dbpass = 'fozBQFMS$YS';
$dbname = 'SLME_test_3';

$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function get_datasets($conn=null)
	{
		if($conn)
			{			
				$queryall="SELECT ID, Dataset, Hover FROM r_Dataset  
					ORDER BY ID ASC LIMIT 1000"; 
				
				//echo $queryall;
					try
						{	
							$data=$conn->query($queryall);
							//$conn->close();
							if($data!=null)
								{	return $data;}
						}
						
						
					catch(Exception $e)
						{
							//echo $e;
							return false;
							//$conn->close();
						}
			}

		return false;	
	}

function get_main_search($conn=null,
		$Datasets = "'1000'",
		$ID_Person = "Null",
		$ID_Source = "Null",
		$SName = "Null",
		$FName = "Null", 
		$FName_Var = "True",
		$Origin = "Null",
		$Rank = "Null",
		$Stat = "Null",		
		$Service = "Null",
		$Captain = "Null",
		$Lieutenant = "Null",
		$Commander = "Null", 		
		$YearMin = "Null",
		$YearMax = "Null",
		$Ref = "Null",
		$orderby="Person_Name",
		$asc="ASC",
		$empty = "'-nil-'"

		)
	{

		/* The list of datasets in use is given in a string where the number of the simbol corresponds to the Dataset ID:
		If the 1st symbol of the string is not 0; the dataset with a_Source.ID = 1 is included into the ; 2nd symbol for a_Source.ID_Dataset = 2 etc.*/
		 /* empty= a value to be entered into search parameters to achieve the rows with empty value of a column*/
		if(empty($Datasets)){$Datasets="'1000'";}
		if(empty($ID_Person)){$ID_Person="Null";}
		if(empty($ID_Source)){$ID_Source="Null";}
		if(empty($SName)){$SName="Null";}
		if(empty($FName)){$FName="Null";}
		if(empty($FName_Var)){$FName_Var="Null";}
		if(empty($Origin)){$Origin="Null";}
		if(empty($Rank)){$Rank="Null";}
		if(empty($Stat)){$Stat="Null";}
		if(empty($Service)){$Service="Null";}
		if(empty($Captain)){$Captain="Null";}
		if(empty($Commander)){$Commander="Null";}
		if(empty($YearMin)){$YearMin="Null";}
		if(empty($YearMax)){$YearMax="Null";}
		if(empty($Ref)){$Ref="Null";}
		if(empty($orderby)){$orderby="Person_Name";}
		if(empty($asc)){$asc="ASC";}	
		
		if($conn)
			{			
				$queryall="SELECT 
						`v_FullTable`.`ID`,
						`v_FullTable`.`Person_Name`,
						`v_FullTable`.`ID_Person`,
						`v_FullTable`.`Origin`,
						`v_FullTable`.`Rank`,
						`v_FullTable`.`Stat`,
						`v_FullTable`.`Service`,
						
						`v_FullTable`.`Captain`,
						`v_FullTable`.`ID_Captain`,
						
						`v_FullTable`.`Lieutenant`,

						`v_FullTable`.`ID_Lieutenant`,
						
						`v_FullTable`.`Commander` 	,

						`v_FullTable`.`ID_Commander`,
						
						`v_FullTable`.`S_Date`,
						`v_FullTable`.`ID_Source`,
						`v_FullTable`.`Source_Type`,
						`v_FullTable`.`Full_Reference`,
						`v_FullTable`.`Link_URL`
						
					FROM `v_FullTable`
						
					WHERE
						(Substring(".$Datasets.", t_ID_Dataset, 1) <> '0') 
							
						AND IF (".$ID_Person." is Null, True, ID_Person = ".$ID_Person.") 
						AND IF (".$ID_Source." is Null, True, ID_Source = ".$ID_Source.")  
						   
						AND IF (".$SName." is Null, True, IF(t_Surname is Null, ".$SName." = ".$empty.", t_Surname like ".$SName.")) 
						AND ( 
							(IF (".$FName." is Null, True, IF(t_First_Name is Null, ".$FName." = ".$empty.", t_First_Name like ".$FName."))) 
									
							OR (  
								(".$FName_Var." = True) AND (NOT(".$FName." is Null)) 
								AND 
								(
									t_ID_Basic_Firstname in (
										Select distinct ID_Basic 
										from r_First_Name 
										WHERE ((Name_Value like ".$FName.") and (not isnull(ID_Basic)))
									)
								) 
							) 
						
						) 
						
						AND IF (".$Rank." is Null, True, IF(Rank is Null, ".$Rank." = ".$empty.", Rank like ".$Rank.")) 
						AND IF (".$Stat." is Null, True, IF(Stat is Null, ".$Stat." = ".$empty.", Stat like ".$Stat.")) 
										 
						AND IF (".$YearMin." is Null, True, LEFT(IFNULL(t_Date_in_Service, IFNULL(t_Service_Year,IFNULL(t_Issue_Date,''))),4)>=".$YearMin.") 
						AND IF (".$YearMax." is Null, True, LEFT(IFNULL(t_Date_in_Service,	IFNULL(t_Service_Year,IFNULL(t_Issue_Date,''))),4)<=".$YearMax.") 
										 
						AND IF (".$Captain." is Null, True, IF(Captain is Null, ".$Captain." = ".$empty.", Captain like ".$Captain.")) 
						AND IF (".$Lieutenant." is Null, True, IF(Lieutenant is Null, ".$Lieutenant." = ".$empty.", Lieutenant like ".$Lieutenant.")) 
						AND IF (".$Commander." is Null, True, IF(Commander is Null, ".$Commander." = ".$empty.", Commander like ".$Commander.")) 
							
						AND IF (".$Origin." is Null, True, IF(Origin is Null, ".$Origin." = ".$empty.", Origin like ".$Origin.")) 
						AND IF (".$Service." is Null, True, IF (Service is Null, ".$Service." = ".$empty.", Service like ".$Service.")) 
						AND IF (".$Ref." is Null, True, IF(Full_Reference is Null, ".$Ref." = ".$empty.", Full_Reference like ".$Ref."))            	
							
						GROUP BY 
						`ID` , 
						`Person_Name` , 
						`ID_Person` , 
						`Origin` , 
						`Rank` , 
						`Stat` , 
						`Service` , 
						`S_Date` , 
						`ID_Source` , 
						`Source_Type` , 
						`Full_Reference` , 
						`Link_URL`
						 ORDER BY ".$orderby.", v_FullTable.ID ".$asc." LIMIT 15000";			
						
						//echo $queryall;
					try
						{	
							$data=$conn->query($queryall);
							//$conn->close();
							if($data!=null)
								{	return $data;}
						}
						
						
					catch(Exception $e)
						{
							//echo $e;
							return false;
							//$conn->close();
						}
			}

		return false;	
	}


function get_rank_list($conn=null)
	{
		if($conn)
			{			
				$queryall="
						SELECT 
							`r_Rank`.`Presentation` AS `rank`
						FROM
							`r_Rank`
						WHERE
							(`r_Rank`.`Is_Standard_Value` = 1);
				"; 
				
				//echo $queryall;
					try
						{	
							$data=$conn->query($queryall);
							//$conn->close();
							if($data!=null)
								{	return $data;}
						}
						
						
					catch(Exception $e)
						{
							//echo $e;
							return false;
							//$conn->close();
						}
			}

		return false;	
	}
	
function get_status_list($conn=null)
	{
		if($conn)
			{			
				$queryall="
					SELECT 
						`r_Status`.`Presentation` AS `status`
					FROM
						`r_Status`
					WHERE
						(`r_Status`.`Is_Standard_Value` IS TRUE)
				"; 
				
				//echo $queryall;
					try
						{	
							$data=$conn->query($queryall);
							//$conn->close();
							if($data!=null)
								{	return $data;}
						}
						
						
					catch(Exception $e)
						{
							//echo $e;
							return false;
							//$conn->close();
						}
			}

		return false;	
	}
	
function getperson($id)
	{
			//include 'dbconnect.php';
			include '../dbopen.php';			
			
			$queryall="SELECT * FROM SLME_test_3.v_Overall WHERE ID='".$id."'"; 
			
			//echo $queryall;
				try
					{	
						$data=$conn->query($queryall);
						//$conn->close();
						if($data!=null)
							{	return $data;}
					}
					
					
				catch(Exception $e)
					{
						//echo $e;
						$conn->close();
						return false;
						
					}


			
	}
	
	
	
	
//   ########################### MAIN PAGE  ##############################
echo "
	<div class=\"container-fluid\" style=\"padding-top: 50px;\">

		<!-- ############## start intro content ############## -->
		<div class=\"container col-sm-12\">
			<div class=\"col-xs-12 col-md-8\">

			  
				<!-- intro text -->
				<div class=\"cell-b20 cell-md-b0\">
					<div class=\"island island-outline\" style=\"background-color:#ffffff\">
						<div class=\"cell-20\" style=\"background-color:#ffffff;padding-top: 1px;padding-left: 1px\">
							<h2 class=\"h3 block-heading block-heading-pull-t50 block-heading-l20\"><b>View Records</b></h2>
							
							
							<!-- start smain search-->
							<form class=\"form-group\" name=\"mainsearchform\" id=\"mainsearchform\" action=\"maindbsearch.php\" method=\"POST\">
							
								<input type='hidden' name='mainsearch' id='mainsearch' value='mainsearch'>
								<input type='hidden' name='id_person' id='id_person' value='id_person'>
								<input type='hidden' name='id_source' id='id_source' value='id_source'>
								<input type='hidden' name='empty' id='empty' value='empty'>	
								<input type='hidden' name='downloadds' id='downloadds' value='0'>	
								
								<input type='hidden' name='orderby' id='orderby' value='
								";
								/*?php*/
								if(!empty($_REQUEST['orderby']))
									{ echo $_REQUEST['orderby'];}
								else{echo "S_Date";}
								/*?>*/
								echo "'>	
								<input type='hidden' name='asc' id='asc' value='";
								/*?php*/
								if(!empty($_REQUEST['asc']))
									{ echo $_REQUEST['asc'];}
								else{echo "ASC";}
								/*?>*/
echo "'>						

<div class=\"container-fluid\" style=\"background-color:#ffffff\"><!-- main search area div  -->


	
		<!--  DATASETS PANEL  -->
		<div class=\"container-fluid\" style=\"background-color:#ffffff\">
		  <h2 class=\"searchheader\"><b>Datasets</b></h2>
		  ";
		  
		  
			//<?php
				//  fill DATASETS with data
				if($dsetsdata)
				{
					echo "<table class='table-condensed table-hover'><tr>";
					$i=0;
					while($row = $dsetsdata->fetch_assoc()) 
						{	
							$i++;
							echo "<td>

								  <input type='checkbox' name='dataset".$i."' id='dataset".$i.
								  "' value='".$row["ID"]."' onchange='setDatasets()' 														  
								  ".$sel_ds[$i-1]." 
								  onMouseOver=\"showhint(this.id,'".$row["Hover"]."')\" 
								  onMouseOut=\"tooltip_hide('tooltip')\"><label>
								  &nbsp;".$row["Dataset"]."</label>
																		
							</td>
							";
						}
					echo "</tr></table>";										
				}
				else{echo "No Datasets!";}
			/*?>*/
			
		echo "
			<input type=\"hidden\" name=\"datasets\" id=\"datasets\" value='";
			
			//?php
			echo $datasets;
			/*?>*/
		echo "	'>
										</div> <!-- close Datasets panel  -->
										
										 <!-- DIV HOLDING SEARCH AND BUTTON CELLS 1 & 2-->	
										<div class=\"container-fluid col-sm-12\" style=\"background-color:#ffffff\">
											
											 <!-- DIV HOLDING LHS CELL ITEMS -->	
											<div class=\"container col-sm-11\" style=\"background-color:#ffffff;padding-left:1px\">
														<h2 class=\"searchheader\"><b>Search</b></h2>										
												 <!-- DIV HOLDING SURNAME AND FIRSTNAME -->	
												<div class=\"container col-sm-12\" style=\"background-color:#ffffff\">
																				
													<div class=\"container col-sm-4\">	
															<input class='test' type=\"text\" name=\"surname\" id=\"surname\" 
															value='";
															//?php
															if(!empty($_REQUEST['surname'])){ echo $_REQUEST['surname'];}
															/*?>*/
															echo "' placeholder=\"Surname\" 
															onMouseOver=\"showhint(this.id,'Enter full or fuzzy name (e.g. Sm*th or Sm%th)')\" 
															onMouseOut=\"tooltip_hide('tooltip')\">
													</div>
													<div class=\"container col-sm-4\">	
															<input class='test' type=\"text\" name=\"fname\" id=\"fname\" onBlur='' 
															value='";
															//?php 
															if(!empty($_REQUEST['fname'])){ echo $_REQUEST['fname'];}
															/*?>*/
															echo "' placeholder=\"First name\"
															onMouseOver=\"showhint(this.id,'Enter full or fuzzy name (e.g. Ed*d or Ed%d for Edward, Edmund)')\" 
															onMouseOut=\"tooltip_hide('tooltip')\">
													</div>
													<div class=\"container col-sm-4\">
														<div class=\"container col-sm-12\">
															<div class=\"container col-sm-11\" style=\"font-size:100%;text-align:left\">														
																<table style=\"border:0px solid black;\">
																<tr>
																	<td style=\"width:90%\">
																	First name variations</td>
																	<td style=\"width:10%\">
																	<input class='test' type=\"checkbox\" name=\"fname_var\" id=\"fname_var\" value='true' ";
																	//?php
																	echo $chkfname_var;
																	/*?>*/ 
																	echo " onMouseOver=\"showhint(this.id,'Check to receive related first names e.g. Steven or Etienne while searching for Stephen')\" 
																	onMouseOut=\"tooltip_hide('tooltip')\">
																	</td>
																</tr>
																</table>
															</div>
																																							
															<div class=\"container col-sm-1\" style=\"text-align:left\">
																&nbsp;
															</div>
														
														</div>
														
													</div>									

												</div> <!-- fn/sn close -->								

												<!-- div holding search options and buttons -->
												<div class=\"container col-sm-12\" style=\"background-color:#ffffff;padding-top:20px;padding-bottom:20px:padding-left: 0px\">
												
														
														<h3 class=\"searchheader\"><b>Advanced</b></h3>
														<table class=\"ptest table-bordered table-hover\">

															<tr>

															<td>
																<input class='test' type=\"text\" name=\"origin\" id=\"origin\" onBlur='' 
																value='";
																
																//?php 
																if(!empty($_REQUEST['origin'])){ echo $_REQUEST['origin'];}
																/*?>*/
																echo "' placeholder=\"Origin\" 
																	onMouseOver=\"showhint(this.id,'Enter place, county (e.g. Bed* for Bedfordshire or Beds), nationality (e.g. %Gasc%)')\" 
																	onMouseOut=\"tooltip_hide('tooltip')\">
															</td>
															<td>
																<select class=\"ddowns\" name=\"status\" id=\"status\" onchange=''>
																	  <option value=\"\">Status</option>
																	  ";
																	  
																	  //?php
																		if(isset($statusdd))
																		{
																			while($row = $statusdd->fetch_assoc())
																				{
																					echo "<option value='".$row['status']."' ";
																					if(!empty($_REQUEST['status']))
																						{
																							if($_REQUEST['status']==$row['status']){echo " selected";}
																						}
																					echo ">".$row['status'];
																					echo "</option>";
																				}												
																		}
																	  
																	  /*?>*/
															echo "		  
																</select>
															</td>

															<td>
																
																<select class=\"ddowns\" name=\"rank\" id=\"rank\" onchange=''>
																	  <option value=\"\">Rank</option>
																	  ";
																	  
																	 // ?php
																		if(isset($rankdd))
																		{
																			while($row = $rankdd->fetch_assoc())
																				{
																					echo "<option value='".$row['rank']."' ";
																					if(!empty($_REQUEST['rank']))
																						{
																							if($_REQUEST['rank']==$row['rank']){echo " selected";}
																						}
																					echo ">".$row['rank'];
																					echo "</option>";
																				}												
																		}
																	  
																	 /*?>*/

															echo "
																</select>
															</td>

															<td>
																<input class='test' type=\"text\" name=\"service\" id=\"service\" 
																value='";
																
																//?php 
																if(!empty($_REQUEST['service'])){echo $_REQUEST['service'];}
																/*?>*/
																
																echo "' placeholder=\"Service\" 
																	onMouseOver=\"showhint(this.id,'Enter fuzzy location (e.g. %Caen% for all mentions of Caen) or type of service (e.g. %gar%)')\" 
																	onMouseOut=\"tooltip_hide('tooltip')\">
															</td>
															<td>
																<input class='test' type=\"text\" name=\"captain\" id=\"captain\" onBlur='' 
																value='";
																//?php
																if(!empty($_REQUEST['captain'])){ echo $_REQUEST['captain'];}
																/*?>*/
																echo "' placeholder=\"Captain\" 
																	onMouseOver=\"showhint(this.id,'Enter fuzzy name / highest title (e.g. %talbot%shrewsbury% )Order is e.g. Montagu, Thomas, earl of Salisbury (%salisbury%)')\" 
																	onMouseOut=\"tooltip_hide('tooltip')\">															
															</td>
															<td>
																<input class='test' type=\"text\" name=\"lieutenant\" id=\"lieutenant\" onBlur='' 
																value='";
																//?php 
																if(!empty($_REQUEST['lieutenant'])){ echo $_REQUEST['lieutenant'];}
																/*?>*/
																echo "' placeholder=\"Lieutenant\"
 																	onMouseOver=\"showhint(this.id,'Enter fuzzy name (e.g. %fastolf%john% )Order is e.g. Fastolf, John, Sir (highest status always given)')\" 
																	onMouseOut=\"tooltip_hide('tooltip')\">																
															</td>
															<td>
																<input class='test' type=\"text\" name=\"commander\" id=\"commander\" onBlur='' 
																value='";
																//?php 
																if(!empty($_REQUEST['commander'])){ echo $_REQUEST['commander'];}
																
																/*?>*/
																echo "' placeholder=\"Commander\" 
																	onMouseOver=\"showhint(this.id,'Enter fuzzy name / highest title (e.g. %talbot%shrewsbury% )Order is e.g. Montagu, Thomas, earl of Salisbury (%salisbury%)')\" 
																	onMouseOut=\"tooltip_hide('tooltip')\">
															</td>
															<td>
																<input class='test' type=\"text\" name=\"yearmin\" id=\"yearmin\" onBlur='' 
																value='";
																//?php
																if(!empty($_REQUEST['yearmin'])){ echo $_REQUEST['yearmin'];}
																/*?>*/
																echo "' placeholder=\"Year from\" 
																	onMouseOver=\"showhint(this.id,'Enter year, e.g. 1415. To search for a particular year put same date in Year from and Year to. Results will be given as YYYYMMDD.')\" 
																	onMouseOut=\"tooltip_hide('tooltip')\">
															</td>
															<td>
																<input class='test' type=\"text\" name=\"yearmax\" id=\"yearmax\" onBlur='' 
																value='";
																//?php
																if(!empty($_REQUEST['yearmax'])){ echo $_REQUEST['yearmax'];}
																/*?>*/
																echo "' placeholder=\"Year to\" 
																	onMouseOver=\"showhint(this.id,'Enter year, e.g. 1415. To search for a particular year put same date in Year from and Year to. Results will be given as YYYYMMDD.')\" 
																	onMouseOut=\"tooltip_hide('tooltip')\">
															</td>
															<td>
																<input class='test' type=\"text\" name=\"ref\" id=\"ref\" onBlur='' 
																value='";
																//?php
																if(!empty($_REQUEST['ref'])){ echo $_REQUEST['ref'];}
																/*?>*/
																echo "' placeholder=\"Ref\" 
																	onMouseOver=\"showhint(this.id,'Enter archival reference ')\" 
																	onMouseOut=\"tooltip_hide('tooltip')\">
															</td>
															</tr>

														</table>
														
													</div>
												
											</div>  <!-- CLose cell 1 div -->	
											
											<!-- CELL 2 DIV -->
											<div class=\"panel-body col-sm-1\"  style=\"padding-top: 10px;padding-left: 1px;padding-right: 1px\">	
											
												<!-- SEARCH BUTTONS PANEL 
												<div class=\"container col-sm-12\" style=\"background-color:#ffee00\">-->

													<div class=\"col-sm-12\">

														<div class=\"form-group\">
															<div class=\"col-sm-12\">										
																<button type=\"button\" class=\"btn btn-default\" name=\"searchsubmit\" onclick=\"validatesearch()\">Search</button>
																<br><br>
															</div>
														</div>
														<div class=\"form-group\">
															<div class=\"col-sm-12\">										
																<button type=\"button\" class=\"btn btn-default\" name=\"clear\" onclick=\"clearform()\">Clear</button>
																<br><br>
															</div>
															
														</div>
														
														<div class=\"form-group\">								
															<div class=\"col-sm-12\">																				
																<button type=\"button\" class=\"btn btn-default\" name=\"dbdownload\" id=\"dbdownload\" onclick='download_ds()'>Download</button>
															</div>												
														</div>											
													</div>										
												 
												<!--</div>  close Search parameters panel -->
												
											</div>  <!-- CLose cell 2 div -->	
										</div>  <!-- CLose 2 cell div -->
								</form><!-- close main search form -->

									<!--      Results table     -->
									<div class=\"container-fluid col-sm-12\" style=\"background-color:#ffffff;padding-top:50px;padding-left: 15px\">
										<h2 class=\"searchheader\"><b>Results table</b></h2>
										";
											//?php  
												if(isset($mainsearch)&& !empty($mainsearch))
													{
														if($mainsearch->num_rows>0)
														{
																echo "<p>".$mainsearch->num_rows." records found</p>";
																echo "<div class='table-responsive'>";
																echo "<table class='table table-bordered table-hover''>
																<tr>
																<th><a href='#' onclick='setorderby(\"Person_Name\")'>Name</a></th>
																<th><a href='#' onclick='setorderby(\"Origin\")'>Origin</a></th>
																<th><a href='#' onclick='setorderby(\"Stat\")'>Status</a></th>
																<th><a href='#' onclick='setorderby(\"Rank\")'>Rank</a></th>
																<th><a href='#' onclick='setorderby(\"Service\")'>Service</a></th>
																<th><a href='#' onclick='setorderby(\"Captain\")'>Captain</a></th>
																<th><a href='#' onclick='setorderby(\"Lieutenant\")'>Lieutenant / Sub-Captain</a></th>
																<th><a href='#' onclick='setorderby(\"Commander\")'>Commander</a></th>
																<th><a href='#' onclick='setorderby(\"S_Date\")'>Service Date</a></th>
																<th><a href='#' onclick='setorderby(\"Source_Type\")'>Source Type</a></th>
																<th><a href='#' onclick='setorderby(\"Full_Reference\")'>Reference</a></th>
																														</tr>";
														
																//RESULTS
																while($row = $mainsearch->fetch_assoc()) {
																			echo "<tr style='font-size:0.66em;'><td>";
																			if(!empty($row['ID_Person'])){ echo "<a href='biosearch.php?id=".$row['ID_Person']."' onclick=''>".$row['Person_Name']."</a>";}
																			else{echo $row["Person_Name"];}
																			echo "</td><td>"
																			.$row["Origin"]."</td><td>"
																			.$row["Stat"]."</td><td>"
																			.$row["Rank"]."</td><td>"
																			.$row["Service"]."</td><td>";
																			echo $row["Captain"];
																			echo "</td><td>";
																			echo $row["Lieutenant"];																	
																			echo "</td><td>";
																			echo $row["Commander"];
																			echo "</td><td>"				
																			.$row["S_Date"]."</td><td>"
																			.$row["Source_Type"]."</td><td>";
																			if(!empty($row['Link_URL'])){ echo "<a href='".$row['Link_URL']."' >".$row['Full_Reference']."</a>";}
																			else{echo $row["Full_Reference"];}																	
																			echo "</td></tr>"
																			;
																		}
																echo "</table>";
															echo "</div>";
														}
														else{echo "<br>No records"; }
													}					
											/*?>*/
										echo "</div>								
									
								</div>						

							
							<!-- end smain search-->				
					
					<!--//.island-->
				</div>
				<!--//.cell-->
						
						</div>	
					<!--<div class=\"cell-md-b50 rule rule-bottom\"> -->
						<h2 class=\"cell-t10 cell-b5 cell-md-t50 accordion__toggle\" data-toggle=\"toggle\" data-target=\"#associated-research-centres\">
						
						
						<!--		COMMENT OUT PROJECT TEAM -RK
						
						
							Project Team
							<span class=\"glyphicon glyphicon-menu-down pull-right visible-xs visible-sm\" aria-hidden=\"true\"></span>
						</h2>
						<ul class=\"list-group-columns-md list-group-plain list-group-links
									rule rule-top rule-none-md
									cell-v10 cell-md-v0
									accordion__item\" id=\"associated-research-centres\">
							<li class=\"list-group-item\"><b class=\"pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5\" role=\"presentation\"></b><a href=\"/research/research-centres-henley.aspx?CdsDrId=the-centre-for-institutional-performance\">Person 1</a></li>
							<li class=\"list-group-item\"><b class=\"pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5\" role=\"presentation\"></b><a href=\"http://www.reading.ac.uk/economic-history/\">Person 2</a></li>
							<li class=\"list-group-item\"><b class=\"pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5\" role=\"presentation\"></b><a href=\"http://www.reading.ac.uk/sustainability-in-the-built-environment/sbe-home.aspx\">Person 3</a></li>
							<li class=\"list-group-item\"><b class=\"pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5\" role=\"presentation\"></b><a href=\"http://www.reading.ac.uk/tsbe/tsbe-home-2.aspx\">Person 4</a></li>
						</ul>
						
					</div>-->
					
						
					
					
				</div>
				<!--//.accordion-->
			</div>
			<!--//.col-->

			<!--//.col-->
		</div>
		<!--//.row-->
		<!-- ############## end intro content ############## -->
	</div>
<!--//.contain -->
</main>


";
?>