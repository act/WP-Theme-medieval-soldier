<?php

function find_files($dir,$ftype='',$filter='') 
{ 
	$farray=[];
	// open directory
	//echo "strlen=".strlen($ftype);  //DEBUG
	
	if (is_dir($dir)) 
		{
			$contents =scandir($dir);
			foreach($contents as $item)
			{
				//	 we dont want the directory we are in or the parent directory 
				if ($item !== "." && $item !== "..")
					{
						// recursively call the function, if we find another directory 
						//echo $dir."/".$item;  //DEBUG
						
						if (is_dir($dir."/".$item)) 
							{
								$farray=array_merge($farray,find_files($dir."/".$item,$ftype,$filter));
							} 
						else
							{
								// not a directory, so apply ftype
								if(strlen($ftype)>0)
									{
										$exts=explode(",",$ftype);
										foreach($exts as  $ext)
											{
												$path_parts=pathinfo($dir."/".$item);
												if($path_parts['extension']==$ext)
													{
														if(strlen($filter)>0)
															{
																//match on filetype now match name filter
																if(substr_count($item,$filter)>0)
																	{
																		array_push($farray,$dir."/".$item);
																	}																
															}
														else{array_push($farray,$dir."/".$item);}									
													}
											}
									}
									// no ftype so log all
								else
									{
										if(strlen($filter)>0)
											{
												//match on filetype now match name filter
												if(substr_count($item,$filter)>0)
													{
														array_push($farray,$dir."/".$item);
													}																
											}
										else{array_push($farray,$dir."/".$item);}
									}
							}
					}
			}
	}
	return $farray;

}


//test
$dir='images';
$mystring = 'abcdefgh';
$findme   = 'cde';
$pos = substr_count($mystring, $findme);
echo "str in item=".$pos;

/*$a1=[1,2,3,4];
$a2=[5,6,7,8];
$a1=array_merge($a1,$a2);
var_dump($a1);


$filter='jpg,tif,png';
echo "is dir=".is_dir($dir);
echo "<br>looking for files in ".$dir."....<br>";
//echo "scan dir=".scandir($dir);
$contents =scandir($dir);
foreach($contents as $item)
			{echo "<br>found..".$item;}
*/
echo "<br>using find_files";
$result=find_files($dir,'','logo');

foreach($result as $item)
			{echo "<br>found..".$item;}
?>