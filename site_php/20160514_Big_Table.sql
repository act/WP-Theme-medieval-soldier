Set 
	/* Search parameters set*/ 
	@Datasets = '111111', 
	/* The list of datasets in use is given in a string where the number of the simbol corresponds to the Dataset ID:
    If the 1st symbol of the string is not 0, the dataset with a_Source.ID = 1 is included into the ; 2nd symbol for a_Source.ID_Dataset = 2 etc.*/
    
    @ID_Person = Null,
    @ID_Source = Null,
    
    @SName = Null,
    @FName = Null, 
    @FName_Var = False,
    
    @Origin = null,
    @Rank = null,
    @Stat = null,
    
    @Service = Null,
    @Captain = Null,
    @Lieutenant = Null,
    @Commander = Null, 
    
    @YearMin = Null,
    @YearMax = Null,
    @Ref = Null,
    @empty = '-nil-'; /*a value to be entered into search parameters to achieve the rows with empty value of a column*/
    
SELECT 
	`a_Person_Record`.`ID` AS `ID`,
	
    CONCAT_WS(', ',
		`a_Person_Record`.`Surname`,
		CONCAT_WS(' ',
			`a_Person_Record`.`First_Name`,
			`a_Person_Record`.`de_la`,
			`a_Person_Record`.`Description`
		),
		`a_Person_Record`.`Occupation`
	) AS `Person_Name`,
	
    `a_Person_Record`.`ID_Person` AS `ID_Person`,
	CONCAT_WS('; ',
		CONCAT_WS(', ',
			`a_Person_Record`.`Origin_Place`,
			`a_Person_Record`.`Origin_Region`
		),
		`a_Person_Record`.`Nation`
	) AS `Origin`,
	
    `r_Rank`.`Presentation` AS `Rank`,
	
    `r_Status`.`Presentation` AS `Stat`,
	
    IF(
		(`a_Retinue`.`Service_Garrison` IS NOT NULL),
		CONCAT_WS(', ',
			CONCAT_WS(' ',
				'Garrison of',
				`a_Retinue`.`Service_Garrison`
			),
			`a_Retinue`.`Service_Engagement`
		),
		CONCAT_WS(', ',
			`r_Service_Type`.`Service_Type`,
			`a_Retinue`.`Service_Region`,
			`a_Retinue`.`Service_Person`,
			`a_Retinue`.`Service_Engagement`
		)
	) AS `Service`,
	
    IF(
		ISNULL(`v_Capt`.`NumLines`),
		'',
		IF(
			(`v_Capt`.`NumLines` = 1),
			TRIM(`v_Capt`.`Off_Name`),
			CONCAT(TRIM(`v_Capt`.`Off_Name`), ' et al.')
		)
	) AS `Captain`,
	
    `v_Capt`.`ID_Person` AS `ID_Captain`,
	
    IF(
		ISNULL(`v_Lieut`.`NumLines`),
		'',
		IF(
			(`v_Lieut`.`NumLines` = 1),
			TRIM(`v_Lieut`.`Off_Name`),
			CONCAT(TRIM(`v_Lieut`.`Off_Name`), ' et al.')
		)
	) AS `Lieutenant`,
	
    `v_Lieut`.`ID_Person` AS `ID_Lieutenant`,
	
    IF(
		ISNULL(`v_Com`.`NumLines`),
		'',
		IF(
			(`v_Com`.`NumLines` = 1),
			TRIM(`v_Com`.`Off_Name`),
			CONCAT(TRIM(`v_Com`.`Off_Name`), ' et al.')
		)
	) AS `Commander`,
	
    `v_Com`.`ID_Person` AS `ID_Commander`,
	
    IFNULL(`a_Retinue`.`Date_in_Service`,
		IFNULL(`a_Retinue`.`Service_Year`,
			IF(
				ISNULL(`a_Source`.`Issue_Date`),
				'',
				CONCAT('[', `a_Source`.`Issue_Date`, ']')
			)
		)
	) AS `S_Date`,
        
	`a_Source`.`ID` AS `ID_Source`,
	
    `r_Source_Type`.`Source_Type` AS `Source_Type`,
        
	CONCAT_WS(', ',
		IFNULL(
			`a_Source`.`Reference`, 
			IFNULL(`a_Source`.`Editions`,'')                                       
		),
		IFNULL(
			`a_Register`.`Source_Location`, 
			IFNULL(`a_Source`.`Reference_Location`,Null)
		)
	) AS `Full_Reference`,
        
	`a_Source`.`Link_URL` AS `Link_URL`
    
    FROM
        (((((((((((`a_Source`
        LEFT JOIN `r_Source_Type` ON ((`r_Source_Type`.`ID` = `a_Source`.`ID_Source_Type`) ))
        LEFT JOIN `a_Register` ON ((`a_Source`.`ID` = `a_Register`.`ID_Source`)))
        LEFT JOIN `a_Person_Record` ON ((`a_Register`.`ID_Record` = `a_Person_Record`.`ID`)))
        LEFT JOIN `r_First_Name` ON ((`r_First_Name`.`ID` = `a_Person_Record`.`ID_Basic_Firstname`)))
        LEFT JOIN `r_Rank` ON ((`r_Rank`.`ID` = `a_Person_Record`.`ID_Rank`)))
        LEFT JOIN `r_Status` ON ((`r_Status`.`ID` = `a_Person_Record`.`ID_Status`)))
        LEFT JOIN `a_Retinue` ON ((`a_Retinue`.`ID` = `a_Register`.`ID_Retinue`)))
        LEFT JOIN `r_Service_Type` ON ((`a_Retinue`.`ID_Service_Type` = `r_Service_Type`.`ID`)))
        LEFT JOIN `v_Retinue_Officers` `v_Capt` ON (((`v_Capt`.`ID_Retinue` = `a_Retinue`.`ID`)
            AND (`v_Capt`.`Role` = 1))))
        LEFT JOIN `v_Retinue_Officers` `v_Lieut` ON (((`v_Lieut`.`ID_Retinue` = `a_Retinue`.`ID`)
            AND (`v_Lieut`.`Role` = 2))))
        LEFT JOIN `v_Retinue_Officers` `v_Com` ON (((`v_Com`.`ID_Retinue` = `a_Retinue`.`ID`)
            AND (`v_Com`.`Role` = 3))))
	WHERE
	    (Substring(@Datasets, a_Source.ID_Dataset, 1) <> '0')
       
        AND IF (@ID_Person is Null, True, a_Person_Record.ID_Person = @ID_Person)
		AND IF (@ID_Source is Null, True, a_Source.ID = @ID_Source)

		AND IF (@SName is Null, True, IF(a_Person_Record.Surname is Null, @SName = @empty, a_Person_Record.Surname like @SName))
        AND (
				(IF (@FName is Null, True, IF(a_Person_Record.First_Name is Null, @FName = @empty, a_Person_Record.First_Name like @FName)))
                OR ( 
					(@FName_Var = True) 
                    AND (r_First_Name.ID_Basic in (Select distinct ID_Basic from r_First_Name WHERE Name_Value like @FName and not isnull(ID_Basic)))
                )
			)
		
		AND IF (@Rank is Null, True, IF(a_Person_Record.ID_Rank is Null, @Rank = @empty, r_Rank.Presentation like @Rank))
		AND IF (@Stat is Null, True, IF(a_Person_Record.ID_Status is Null, @Stat = @empty, r_Status.Presentation like @Stat))
		
        AND IF (@YearMin is Null, True, LEFT(IFNULL(a_Retinue.Date_in_Service,	IFNULL(a_Retinue.Service_Year,IFNULL(a_Source.Issue_Date,''))),4)>=@YearMin)
        AND IF (@YearMax is Null, True, LEFT(IFNULL(a_Retinue.Date_in_Service,	IFNULL(a_Retinue.Service_Year,IFNULL(a_Source.Issue_Date,''))),4)<=@YearMax)
        
        AND IF (@Captain is Null, True, IF(v_Capt.Off_Name is Null, @Captain = @empty, v_Capt.Off_Name like @Captain))
        AND IF (@Lieutenant is Null, True, IF(v_Lieut.Off_Name is Null, @Lieutenant = @empty, v_Lieut.Off_Name like @Lieutenant))
		AND IF (@Commander is Null, True, IF(v_Com.Off_Name is Null, @Commander = @empty, v_Com.Off_Name like @Commander))
        
Having
	IF (@Origin is Null, True, IF(Origin is Null, @Origin = @empty, Origin like @Origin))
    AND IF (@Service is Null, True, IF (Service is Null, @Service = @empty, Service like @Service))
    AND IF (@Ref is Null, True, IF(Full_Reference is Null, @Ref = @empty, Full_Reference like @Ref))           
	