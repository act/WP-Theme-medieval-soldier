<?php

include "../dbopen.php";
include "get_datasets.php";
include "get_main_search.php";
include "get_status_list.php";
include "get_rank_list.php";
include "get_person_links.php";
include "getrecord.php";

$dsetsdata=get_datasets($conn);  //get fields
$dsetrows=$dsetsdata->num_rows;
$sel_ds=[];  // chkbx defaults
/*
while($row=dsetsdata->fetch_assoc())
	{
		$dsetstr=$dsetstr.$row['ID'];
		array_push($sel_ds,"checked");
	}
*/

$dsetsstr=str_pad("",$dsetrows,1);  //set default dataset to length of rows
for($i=0;$i<$dsetrows;$i++){array_push($sel_ds,"checked");}  //set chkbx defaults
$datasets=$dsetsstr;

$id_person="Null";
$id_source="Null";
$sname="Null";
$fname="Null"; 
$fname_var="Null";
$origin="Null";
$rank="Null";
$stat="Null";		
$service="Null";
$captain="Null";
$lieutenant="Null";
$commander="Null"; 		
$yearmin="Null";
$yearmax="Null";
$ref="Null";
$orderby="Person_Name";
$asc="ASC";
$pid="";

$linkdata=null;
$biodata=null;

// get person data from View Records Page

if(isset($_REQUEST['pid']))
	{
		// get data for bio panel
		$pid=$_REQUEST['pid'];
		$biodata=getrecord($pid);
		$linkdata=get_person_links($pid);
	} 

if(isset($_GET['id']))
	{
		// get data for bio panel
		$pid=$_GET['id'];
		$biodata=getrecord($pid);
		$linkdata=get_person_links($pid);
	} 


if(isset($_REQUEST['datasets']))
	{
		// get data for datasets panel
		$dsetsstr=$_REQUEST['datasets'];
		$datasets=$_REQUEST['datasets'];
		$split_ds=str_split($dsetsstr);
		$dslen=count($split_ds);
		for($i=0;$i<$dslen;$i++)
			{
				if($split_ds[$i]==1){$sel_ds[$i]="checked";}
				else{$sel_ds[$i]="";}
			}
	}    
$statusdd=get_status_list($conn);
$rankdd=get_rank_list($conn);
if(isset($_REQUEST))
	{
		if(!empty($_REQUEST['surname'])){$surname="'".$_REQUEST['surname']."'";}else{$surname="Null";}
		if(!empty($_REQUEST['fname'])){$fname="'".$_REQUEST['fname']."'";}else{$fname="Null";}
		if(!empty($_REQUEST['fname_var'])&& $_REQUEST['fname_var']=='true'){$chkfname_var='checked';$fname_var="true";}else{$chkfname_var='';$fname_var="false";}
		if(!empty($_REQUEST['origin'])){$origin="'".$_REQUEST['origin']."'";}else{$origin="Null";}
		if(!empty($_REQUEST['rank'])){$rank="'".$_REQUEST['rank']."'";}else{$rank="Null";}
		if(!empty($_REQUEST['status'])){$status="'".$_REQUEST['status']."'";}else{$status="Null";}
		if(!empty($_REQUEST['service'])){$service="'".$_REQUEST['service']."'";}else{$service="Null";}
		if(!empty($_REQUEST['captain'])){$captain="'".$_REQUEST['captain']."'";}else{$captain="Null";}
		if(!empty($_REQUEST['lieutenant'])){$lieutenant="'".$_REQUEST['lieutenant']."'";}else{$lieutenant="Null";}
		if(!empty($_REQUEST['commander'])){$commander="'".$_REQUEST['commander']."'";}else{$commander="Null";}
		if(!empty($_REQUEST['yearmax'])){$yearmax="'".$_REQUEST['yearmax']."'";}else{$yearmax="Null";}
		if(!empty($_REQUEST['yearmin'])){$yearmin="'".$_REQUEST['yearmin']."'";}else{$yearmin="Null";}
		if(!empty($_REQUEST['ref'])){$ref="'".$_REQUEST['ref']."'";}else{$ref="Null";}
		if(!empty($_REQUEST['orderby'])){$orderby=$_REQUEST['orderby'];}else{$orderby="S_Date";}
		if(!empty($_REQUEST['asc'])){$asc=$_REQUEST['asc'];}else{$asc="ASC";}
	}

$empty = "'-nil-'";

//foreach($_REQUEST as $key=>$value){echo $_REQUEST[$key]."=".$value;} 
/*
		$Datasets,
		$ID_Person,
		$ID_Source,
		$SName,
		$FName, 
		$FName_Var,
		$Origin,
		$Rank,
		$Stat,		
		$Service,
		$Captain,
		$Lieutenant,
		$Commander, 		
		$YearMin,
		$YearMax,
		$Ref,
		$empty
*/
if(isset($_REQUEST['biosearch']))
	{
		foreach($_REQUEST as $key=>$value){$_REQUEST[$key]=mysqli_real_escape_string($conn,$value);}
		
		$mainsearch=get_main_search
		(
			$conn,
			"'".$_REQUEST['datasets']."'",
			"Null",
			"Null",
			$surname,
			$fname,
			$fname_var,
			$origin,
			$rank,
			$status,		
			$service,
			$captain,
			$lieutenant,
			$commander, 		
			$yearmin,
			$yearmax,
			$ref,
			$orderby,
			$asc,
			$empty
		);
	}
else
	{
		$mainsearch=get_main_search
			(
			$conn,
			$datasets,
			"'".$pid."'",
			"Null"
			);
	}
//
	
if(isset($_REQUEST['downloadds']))
	{
		if($_REQUEST['downloadds']==1)
			{
				$_REQUEST['downloadds']=0;
				// create csv for download
				if($mainsearch)
					{
						$fstr="downloads/download_ds.csv";
						$f=fopen($fstr,'w');
						if($f)
							{
								$line="Name,Origin,Rank,Status,Service,Captain,Lieutenant,";
								$line.="Commander,Service Date,Source Type,Reference\r\n";
								fwrite($f,$line);
								
								while($row=$mainsearch->fetch_assoc())
									{
										$line="\"".$row["Person_Name"]."\",\"".$row["Origin"]."\",\"".$row["Rank"]."\",\""
										.$row["Stat"]."\",\"".$row["Service"]."\",\"".$row['Captain']."\",\"".$row['Lieutenant']."\",\""
										.$row['Commander']."\",\"".$row["S_Date"]."\",\"".$row["Source_Type"]."\",\"".$row["Full_Reference"]."\"\r\n";
										fwrite($f,$line);
									}
								fclose($f);

								if (file_exists($fstr)) {
									header('Content-Description: File Transfer');
									header('Content-Type: application/octet-stream');
									header('Content-Disposition: attachment; filename="'.basename($fstr).'"');
									header('Expires: 0');
									header('Cache-Control: must-revalidate');
									header('Pragma: public');
									header('Content-Length: ' . filesize($fstr));
									readfile($fstr);
								}
								
								unlink($fstr);
							}
					}
					
					
			}  		
		}		
		
	
?>
<!DOCTYPE html>
<!--[if lte IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> 
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' /><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="Henley Business School">



<link rel="canonical" href="" />

<title>Medieval Soldier</title>

<!-- Bootstrap core CSS -->
<link href="/css/henley-business-school-bootstrap.css" rel="stylesheet">

<script src="/js/head.js"></script>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Favicons -->
<link rel="apple-touch-icon" href="/apple-touch-icon.png">
<link rel="icon" href="/favicon.ico">

<style>
.test
	{
		width:90%;
	}
.ptest
{
	width:95%;
}
.ptest2
{
	width:95%;
}
.ddowns 
	{
		color:black;
	}
</style>

<script>
function showNames(str) {
  var xhttp;
  if (str.length < 2) { 
    document.getElementById("txtHint").innerHTML = "";
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      document.getElementById("txtHint").innerHTML = xhttp.responseText;
    }
  };
  xhttp.open("GET", "getnames.php?q="+str, true);
  xhttp.send();   
}


function validatesearch()
	{
		// block last download signal
		document.getElementById('downloadds').value=0;
		
		var fname=document.getElementById('fname').value;
		document.getElementById('fname').value=filterStr(fname);
		//alert(document.getElementById('fname').value);
		
		var sname=document.getElementById('surname').value;
		document.getElementById('surname').value=filterStr(sname);
		
		//var fname_var=document.getElementById('fname_var').value;
		
		var origin=document.getElementById('origin').value;
		document.getElementById('origin').value=filterStr(origin);		
		
		var captain=document.getElementById('captain').value;	
		document.getElementById('captain').value=filterStr(captain);	
		
		var lieutenant=document.getElementById('lieutenant').value;
		document.getElementById('lieutenant').value=filterStr(lieutenant);
		
		var commander=document.getElementById('commander').value;
		document.getElementById('commander').value=filterStr(commander);		
		
		var service=document.getElementById('service').value;
		document.getElementById('service').value=filterStr(service);
		
		var yearmin=document.getElementById('yearmin').value;
		
		var yearmax=document.getElementById('yearmax').value;
		
		var ref=document.getElementById('ref').value;	
		document.getElementById('ref').value=filterStr(ref);
		
		if(!(yearmax<2050 && yearmax>500 || isInteger(yearmax) || yearmax==""))
			{
				alert("Year to must be a number or empty!");
				document.getElementById('yearmax').value="";
				return false;
			}
		if(!(yearmin<2050 && yearmin>500 || isInteger(yearmin) || yearmin==""))
			{
				alert("Year from must be a number or empty!");
				document.getElementById('yearmin').value="";
				return false;
			}
			
		document.getElementById('biosearchform').submit();
		return true;
		
	}

function isInteger(n)
{
    var result=true;
    if(n==""){result=false;}
    if(n.indexOf('.')>-1){result=false;}
    if(isNaN(n)==true){result=false;}	
    // alert("result="+result);
	return result;
}
	


function filterStr(str)
	{
		if(str.length<1){return '';}
		var str1=str.replace(/['"<>/]/g,'_');
		var str2=str1.replace(/\*/g,'%');
		var str3=str2.replace(/\?/g,'_');	
		//alert('str=' +str+'  str1=' +str1+'  2=' +str2+'  3=' +str3);
		return str3;	
	}
	
	
function setDatasets()
	{
		var dsets=document.getElementById('datasets');  //get dataset string
		var lends=dsets.value.length;
		var newdsstr='';
		//loop over checkboxes
		for(var i=1;i<lends+1;i++)
			{
				var cbxid='dataset'+String(i);
				//alert('cbxid='+cbxid);
				var cbx=document.getElementById(cbxid);
				// build new dsets string
				if(cbx.checked==true){newdsstr=newdsstr+'1';}
				else{newdsstr=newdsstr+'0';}
			}
		dsets.value=newdsstr;
		//alert(dsets.value);
		
	}

function setorderby(field)	
	{
		var orderby=document.getElementById('orderby');
		var asc=document.getElementById('asc');
		orderby.value=field;
		if(asc.value=='ASC'){asc.value='DESC';}
		else{asc.value='ASC';}
		//alert(orderby.value + ' ' + asc);
		var mainfrm=document.getElementById('biosearchform');
		mainfrm.submit();
		return true;
	}
	
function clearform()
	{
		document.getElementById('fname').value='';
		document.getElementById('surname').value='';
		document.getElementById('fname_var').checked=false;
		document.getElementById('rank').selectedIndex=0;
		document.getElementById('status').selectedIndex=0;
		document.getElementById('origin').value='';
		document.getElementById('captain').value='';		
		document.getElementById('lieutenant').value='';	
		document.getElementById('commander').value='';	
		document.getElementById('service').value='';	
		document.getElementById('yearmin').value='';
		document.getElementById('yearmax').value='';		
		document.getElementById('ref').value='';
	}		
	
function gotoref(fid,sid)	
	{
		var formid='gotorefform'+fid;
		var pidelname='pid'+fid;
		//alert(formid);
		//alert(pidelname);
		var f=document.getElementById(formid);
		var pidel=document.getElementById(pidelname);
		pidel.value=sid;
		//alert(f.id);
		f.submit();
		return true;
	}	


function download_ds()
	{
		var mainfrm=document.getElementById('biosearchform');
		var dl=document.getElementById('downloadds');
		dl.value=1;
		//alert(dl.value);
		mainfrm.submit();
		dl.value=0;
		
	}	
</script>



<script src="//use.typekit.net/alk7sin.js"></script>
<script>try{Typekit.load();}catch(e){}</script>

</head>
<body>
<a id="skippy" class="sr-only sr-only-focusable" href="#content">
    <div class="container"><span class="skiplink-text">Skip to main content</span></div>
</a>

<div class="dropdown navbar-dropdown navbar-dropdown-menu">
<div
class="navbar-fixed navbar-fixed-top">
    <nav class="navbar-inverse navbar">
        <div class="container navbar-container">
            <a class="navbar-brand navbar-brand-hbs" href="/">Henley Business School</a>

            <div class="dropdown navbar-dropdown">
<!-- Search toggle -->
                <button class="navbar-right navbar-toggle search-toggle" id="search-toggle" aria-label="Show search form" data-toggle-search data-target="#search-form" data-toggle-group="main-nav" >
                    <b aria-hidden="true" class="glyphicon glyphicon-search"></b>
                    <span class="icon-bar" aria-hidden="true"></span>
                    <span class="icon-bar" aria-hidden="true"></span>
                    <span class="icon-bar" aria-hidden="true"></span>
                    <span class="navbar-toggle-close">&times;</span>
                </button>
                <!-- Search form -->
                <div class="search" id="search-form">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-8 col-md-offset-4 col-lg-7 col-lg-offset-5">
                                <form class="search-form" role="search" aria-labelledby="search-toggle" autocomplete="off" autocapitalize="off">
                                    <input type="text" name="query" id="searchQuery" class="form-control search-control query noSubmit" placeholder="Search Henley">
                                    <button type="submit" class="search-submit btn btn-default btn-primary" data-search-submit>
                                        <b aria-hidden="true" class="glyphicon glyphicon-search"></b>
                                        <span>Search</span>
                                    </button>
                                </form>
                            </div>
                        </div>
                        <!--//.row-->
                        <div class="row">
                            <div class="col-xs-12 search-wrapper">
                                <!-- Search results -->
                                <div class="search-results" id="searchResults">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dropdown navbar-dropdown navbar-dropdown-menu">



<!-- Mobile navigation toggle -->
                <button type="button" class="navbar-toggle" data-toggle-group="main-nav" id="navbar-toggle-mobile" data-target="#mobile-nav" data-toggle-mobilenav>
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar" aria-hidden="true"></span>
                    <span class="icon-bar" aria-hidden="true"></span>
                    <span class="icon-bar" aria-hidden="true"></span>
                    <span class="navbar-toggle-close">&times;</span>
                </button>
                <!-- Mobile navigation -->
                <nav class="navbar-nav-mobile-wrapper island-gray-darker" id="mobile-nav" aria-labelledby="navbar-toggle">
                    <ul class="nav nav-arrow-right navbar-nav navbar-nav-mobile nav-dark">
                        <li class="dropdown">
                            <!-- active -->
                            <a href="#" data-toggle="dropdown" data-target="#" id="subnav1-toggle">Medieval Soldier</a>
                            <!-- <span class="sr-only">(current)</span> -->
                            <ul class="nav nav-sub nav-light nav-arrow-right" id="subnav-1" aria-labelledby="subnav1-toggle">
                                <li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1" data-toggle="dropdown" data-target="#subnav-1">Back</a></li>
                                <li class="dropdown">
                                    <a href="#subnav-1-subnav-1" data-toggle="dropdown" data-target="#subnav-1-subnav-1">The Team</a>
                                    <ul id="subnav-1-subnav-1" class="nav nav-sub nav-light nav-arrow-right">
                                        <li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1-subnav-1" data-toggle="dropdown" data-target="#subnav-1-subnav-1">Back</a></li>
                                        <li><a href="/team/a-curry.html" data-close-mobilenav>Professor Anne Curry</a></li>
                                        <li><a href="/team/a-bell.html" data-close-mobilenav>Dr Adrian R Bell</a></li>
                                        <li><a href="/team/a-king.html" data-close-mobilenav>Dr Andy King</a></li>
                                        <li><a href="/team/d-simpkin.html" data-close-mobilenav>Dr David Simpkin</a></li>
                                        <li><a href="/team/a-chapman.html" data-close-mobilenav>Adam Chapman</a></li>
										<li><a href="/team/team-publications.html" data-close-mobilenav>Team Publications</a></li>
										<li><a href="/team/advisory-board.html" data-close-mobilenav>Advisory Board</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#subnav-1-subnav-2" data-toggle="dropdown" data-target="#subnav-1-subnav-2">Database</a>
                                    <ul id="subnav-1-subnav-2" class="nav nav-sub nav-light nav-arrow-right">
                                        <li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1-subnav-2" data-toggle="dropdown" data-target="#subnav-1-subnav-2">Back</a></li>
                                        <li><a href="/database/database.php" data-close-mobilenav>Search the Database</a></li>
										<li><a href="/database/database-information.html" data-close-mobilenav>Database Information</a></li>
										<li><a href="/database/search-tips.html" data-close-mobilenav>Search Tips</a></li>
										<li><a href="/database/citation.html" data-close-mobilenav>Citation</a></li>
                                        <li><a href="/database/source-info.html" data-close-mobilenav>Source Information</a></li>
										
                                           </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#subnav-1-subnav-3" data-toggle="dropdown" data-target="#subnav-1-subnav-3">Guidance Notes</a>
                                    <ul id="subnav-1-subnav-3" class="nav nav-sub nav-light nav-arrow-right">
                                        <li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1-subnav-3" data-toggle="dropdown" data-target="#subnav-1-subnav-3">Back</a></li>
                                        <li><a href="/guidance/normandygarrisons.html" data-close-mobilenav>Did your ancestor serve in the garrisons of Normandy 1415-1450?</a></li>
                                        <li><a href="/guidance/agincourt">Was your ancestor on the Agincourt campaign with Henry V?</a></li>
                                        
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#subnav-1-subnav-4" data-toggle="dropdown" data-target="#subnav-1-subnav-4">Soldier Profiles</a>
                                    <ul id="subnav-1-subnav-4" class="nav nav-sub nav-light nav-arrow-right">
                                        <li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1-subnav-4" data-toggle="dropdown" data-target="#subnav-1-subnav-4">Back</a></li>
                                        <li><a href="/profles/soldier-profiles.html" data-close-mobilenav>Profles</a></li>
                                        
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#subnav-1-subnav-5" data-toggle="dropdown" data-target="#subnav-1-subnav-5">Dissemination</a>
                                    <ul id="subnav-1-subnav-5" class="nav nav-sub nav-light nav-arrow-right">
                                        <li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1-subnav-5" data-toggle="dropdown" data-target="#subnav-1-subnav-5">Back</a></li>
                                        <li><a href="/dissemination/dissemination.html" data-close-mobilenav>Dissemination to Date</a></li>
                                        
                                        
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#subnav-1-subnav-6" data-toggle="dropdown" data-target="#subnav-1-subnav-6">Contributions</a>
                                    <ul id="subnav-1-subnav-6" class="nav nav-sub nav-light nav-arrow-right">
                                        <li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1-subnav-6" data-toggle="dropdown" data-target="#subnav-1-subnav-6">Back</a></li>
                                        <li><a href="/contributions/contributions.html" data-close-mobilenav>Contributions</a></li>
                                    
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" data-target="#" id="subnav2-toggle">Medieval Interest</a>
                            <ul class="nav nav-sub nav-light nav-arrow-right" id="subnav-2" aria-labelledby="subnav2-toggle">
                                <li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-2" data-toggle="dropdown" data-target="#subnav-2">Back</a></li>
                                <li class="dropdown">
                                    <a href="#subnav-2-subnav-1" data-toggle="dropdown" data-target="#subnav-2-subnav-1">Calculator</a>
                                    <ul id="subnav-2-subnav-1" class="nav nav-sub nav-light nav-arrow-right">
                                        <li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-2-subnav-1" data-toggle="dropdown" data-target="#subnav-2-subnav-1">Back</a></li>
                                        <li><a href="/research" data-close-mobilenav>Calculator</a></li>
                                        <li><a href="/research/research-centres" data-close-mobilenav>About</a></li>
                                        
                                    </ul>
                                </li>
                                
                        
                        <li class="dropdown secondary">
                            <a href="http://www.reading.ac.uk/">University of Reading</a>
							<a href="http://www.southampton.ac.uk/">University of Southampton</a>
                        </li>
                        <!--         <li class="dropdown secondary">
            <a href="/about-us/online-resources-log-in/">Log in</a>
        </li> -->
                    </ul>
                    
                </nav>
            </div>
            <!-- Desktop navigation -->
            <nav class="navbar-right navbar-nav-desktop">
                <div class="cell-v5 clearfix">
                    <div class="btn-group btn-group-bar pull-right text-muted" role="group">
                        <a href="https://www.reading.ac.uk" class="btn btn-plain text-small ">University of Reading</a>
                        <!-- <a href="#" class="btn btn-plain text-small ">Alumni</a> -->
                        <!-- <a href="#" class="btn btn-plain text-small ">International</a> -->
                        <a href="http://www.southampton.ac.uk" class="btn btn-plain text-small">University of Southampton</a>
						
                        
                
                        
                    </div>
                </div>
                <ul class="nav navbar-nav" id="desktop-nav">
                    <li>
                        <!--  class="active" -->
                        <a href="#subnav-desk-1" data-toggle-desktopnav data-target="#subnav-desk-1" data-toggle-group="main-nav">Medieval Soldier</a>
                        <!--span class="sr-only">(current)</span-->
                    </li>
                    <li>
                        <a href="#subnav-desk-2" data-toggle-desktopnav data-target="#subnav-desk-2" data-toggle-group="main-nav">Medieval Interest</a>
                    </li>
                    
                </ul>
            </nav>
        </div>
        <!-- /.container-fluid -->
    </nav>
    <!-- Desktop sub navigation -->
    <ul class="nav nav-sub nav-sub-desktop" id="subnav-desk-1">
        <!-- Subnav 1 -->
        <li class="dropdown">
            <a href="#subnav-desk-1-subnav-desk-1" data-toggle="toggle" data-toggle-group="subnav-desk-1" data-target="#subnav-desk-1-subnav-1" data-toggle-min="1" class="active">The Team</a>
            <ul id="subnav-desk-1-subnav-1" class="nav nav-sub open">
                <li><a href="/team/a-curry.html" data-close-desktopnav>Professor Anne Curry</a></li>
                <li><a href="/team/a-bell.html" data-close-desktopnav>Dr Adrian R Bell</a></li>
                <li><a href="/team/a-king.html" data-close-desktopnav>Dr Andy King</a></li>
                <li><a href="/team/d-simpkin.html" data-close-desktopnav>Dr David Simpkin</a></li>
                <li><a href="/team/a-chapman.html" data-close-desktopnav>Adam Chapman</a></li>
				<li><a href="/team/team-publications.html" data-close-desktopnav>Team Publications</a></li>
				<li><a href="/team/advisory-board.html" data-close-desktopnav>Advisory Board</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#subnav-desk-1-subnav-2" data-toggle="toggle" data-toggle-group="subnav-desk-1" data-target="#subnav-desk-1-subnav-2" data-toggle-min="1">Database</a>
            <ul id="subnav-desk-1-subnav-2" class="nav nav-sub">
										<li><a href="/database/database.php" data-close-desktopnav>Search the Database</a></li>
										<li><a href="/database/database-information.html" data-close-desktopnav>Database Information</a></li>
										<li><a href="/database/search-tips.html" data-close-desktopnav>Search Tips</a></li>
										<li><a href="/database/citation.html" data-close-desktopnav>Citation</a></li>
                                        <li><a href="/database/source-info.html" data-close-desktopnav>Source Information</a></li>
                
				
                
            </ul>
        </li>
        <li class="dropdown">
            <a href="#subnav-desk-1-subnav-3" data-toggle="toggle" data-toggle-group="subnav-desk-1" data-target="#subnav-desk-1-subnav-3" data-toggle-min="1">Guidance Notes</a>
            <ul id="subnav-desk-1-subnav-3" class="nav nav-sub">
                <li><a href="/guidance/normandygarrisons.html" data-close-desktopnav>Did your ancestor serve in the garrisons of Normandy 1415-1450?</a></li>
                <li><a href="/guidance/agincourt.html" data-close-desktopnav>Was your ancestor on the Agincourt campaign with Henry V?</a></li>
                
            </ul>
        </li>
        <li class="dropdown">
            <a href="#subnav-desk-1-subnav-4" data-toggle="toggle" data-toggle-group="subnav-desk-1" data-target="#subnav-desk-1-subnav-4" data-toggle-min="1">Soldier Profiles</a>
            <ul id="subnav-desk-1-subnav-4" class="nav nav-sub">
                <li><a href="/profiles/soldier-profiles.html" data-close-desktopnav>Profiles</a></li>
                
            </ul>
        </li>
        <li class="dropdown">
            <a href="#subnav-desk-1-subnav-5" data-toggle="toggle" data-toggle-group="subnav-desk-1" data-target="#subnav-desk-1-subnav-5" data-toggle-min="1">Dissemination</a>
            <ul id="subnav-desk-1-subnav-5" class="nav nav-sub">
                <li><a href="/dissemination/dissemination.html" data-close-desktopnav>Dissemination 09/06 to date</a></li>
                
            </ul>
        </li>
        <li class="dropdown">
            <a href="#subnav-desk-1-subnav-6" data-toggle="toggle" data-toggle-group="subnav-desk-1" data-target="#subnav-desk-1-subnav-6" data-toggle-min="1">Contributions</a>
            <ul id="subnav-desk-1-subnav-6" class="nav nav-sub">
                <li><a href="/contributions/contributions.html" data-close-desktopnav>Contributions</a></li>
                
            </ul>
        </li>
    </ul>
    <ul class="nav nav-sub nav-sub-desktop" id="subnav-desk-2">
        <!-- Subnav 2 -->
        <li class="dropdown">
            <a href="#subnav-desk-2-subnav-desk-1" data-toggle="toggle" data-toggle-group="subnav-desk-2" data-target="#subnav-desk-2-subnav-1" class="active" data-toggle-min="1">Calculator</a>
            <ul id="subnav-desk-2-subnav-1" class="nav nav-sub open">
                <li><a href="/research" data-close-desktopnav>Calculator</a></li>
                
            </ul>
        </li>
        <li class="dropdown">
            <a href="#subnav-desk-2-subnav-2" data-toggle="toggle" data-toggle-group="subnav-desk-2" data-target="#subnav-desk-2-subnav-2" data-toggle-min="1">About</a>
            <ul id="subnav-desk-2-subnav-2" class="nav nav-sub">
                <li><a href="/about-us/faculty-directory-a-d/" data-close-desktopnav>About</a></li>
                
            </ul>
        </li>
        
</div>
      
        </div><!-- /.container-fluid -->
    </nav>
</div>
<main role="main" id="content">
    <!-- hero -->
<div class="cell-v30 jumbotron island island-gray-dark jumbotron-img" style="background-image: url('/images/medieval-soldiers.png')">
    <div class="container">
        <div class="row">
    <div class="col-xs-6">
        <h1 class="block-heading block-heading-l5 block-heading-b5 block-heading-md-l-reset cell-md-t20">
            <b>Medieval Soldier</b>
        </h1>
    </div><!--//.col-->
</div><!--//.row-->

    </div>
    <!--//.container-->
</div>
<!--//.island-->
<!-- include hbs/news/featured.html -->
<div class="island-outline outline-v">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-back">
                <!-- server side backlink, visible on mobile only-->
                <a href="#" aria-label="Go back up one level">
                    <b class="glyphicon glyphicon-menu-left" aria-hidden="true"></b>
                </a>
            </li>
            <li class="breadcrumb-path">
                <a href="/index.html">Home</a>
            </li>
			
			<!-- 
			Comment Out Breadcrums RK 
			
			<li class="breadcrumb-path">
                <a href="#">Database</a>
            </li>
            <li class="breadcrumb-parent">
                <a href="#">Sub-section</a>
				
            </li>-->
            <li class="active breadcrumb-current">
                <span>Current page</span>
            </li>
			
        </ol><!--//.breadcrumb-->
		</div><!--//.container-->
</div><!--//.island-outline-->

<div class="container cell-v50 cell-md-t60">
    <!-- ############## start intro content ############## -->
    <div class="row">
        <div class="col-xs-12 col-md-8">

          
            <!-- intro text -->
            <div class="cell-b20 cell-md-b0">
                <div class="island island-outline">
                    <div class="cell-20">
                        <h2 class="h3 block-heading block-heading-pull-t50 block-heading-l20"><b>Soldier Profile</b></h2>
						
						
						<!-- BIOGRAPHY & AUTHOR-->
						<div class="container">
							<b>Biography</b>
							<?php
								//echo "pid=".$pid;
								if(isset($biodata))
									{
										if($biodata->num_rows>0)
										{
											$b=$biodata->fetch_assoc();
											echo "<h4>".$b['Longname']."</h4>";

											echo "<p>".$b['Bio']."</p>
												<p>".$b['Bio_Author']."</p>
												";										
										}
									}
								else{echo "<br>No bio data";}
							?>
						</div>	

								
						<!-- start smain search-->
						<form class="form-group" name="biosearchform" id="biosearchform" action="biosearch.php" method="POST">
						
							<input type='hidden' name='biosearch' id='biosearch' value='biosearch'>
							<input type='hidden' name='id_person' id='id_person' value='id_person'>
							<input type='hidden' name='id_source' id='id_source' value='id_source'>
							<input type='hidden' name='empty' id='empty' value='empty'>	
							<input type='hidden' name='downloadds' id='downloadds' value='0'>
							
							<input type='hidden' name='orderby' id='orderby' value='<?php if(!empty($_REQUEST['orderby']))
								{ echo $_REQUEST['orderby'];}
							else{echo "ID";}
							?>'>	
							<input type='hidden' name='asc' id='asc' value='<?php if(!empty($_REQUEST['asc']))
								{ echo $_REQUEST['asc'];}
							else{echo "ASC";}
							?>'>						
							<div class="container">
								<div class="panel panel-default">
								
									<div class="panel-body col-sm-3">
										&nbsp;
									</div> <!-- close spacer panel  -->
									
									


								
								</div> <!-- panel close -->
								
									<div class="panel-body col-sm-12">
									  <h2 class="searchheader"><b>Datasets</b></h2>
										<?php
											//  fill DATASETS with data
											if($dsetsdata)
											{
												echo "<table class='table-condensed table-hover'><tr>";
												$i=0;
												while($row = $dsetsdata->fetch_assoc()) 
													{	
														$i++;
														echo "<td>

															  <label><input type='checkbox' name='dataset".$i."' id='dataset".$i.
															  "' value='".$row["ID"]."' onchange='setDatasets()' ".$sel_ds[$i-1].">
															  &nbsp;".$row["Dataset"]."</label>
																									
														</td>
														";
													}
												echo "</tr></table>";										
											}
											else{echo "No Datasets!";}
										?>
									<input type="hidden" name="datasets" id="datasets" value='<?php echo $datasets;?>'>
									</div> <!-- close Datasets panel  -->
															
								<div class="container col-sm-12">
									<h2 class="searchheader"><b>Search parameters</b></h2>
									<table class="ptest table-bordered table-hover">
<!--										<tr>
											<th>
												Surname
											</th>
											<th>
												Origin
											</th>
											<th>
												Rank
											</th>
											<th>
												Status
											</th>
											<th>
												ServiceFirst Name
											</th>
											<th>
												Captain
											</th>
											<th>
												Lieutenant
											</th>
											<th>
												Commander
											</th>											
											<th>
												Year From
											</th>
											<th>
												&nbsp;
											</th>
											<th>
												Reference
											</th>
										</tr>
-->
										<!-- row 2 -->
										<tr>
										<td>
											<input class='test' type="text" name="surname" id="surname" onBlur='' 
											value='<?php if(!empty($_REQUEST['surname'])){ echo $_REQUEST['surname'];}?>' placeholder="Surname">

										</td>
										<td>
											<input class='test' type="text" name="origin" id="origin" onBlur='' 
											value='<?php if(!empty($_REQUEST['origin'])){ echo $_REQUEST['origin'];}?>' placeholder="Origin">

										</td>
										<td>
											<select class="ddowns" name="rank" id="rank" onchange=''>
												  <option value="">Rank</option>
												  <?php
													if(isset($rankdd))
													{
														while($row = $rankdd->fetch_assoc())
															{
																echo "<option value='".$row['rank']."' ";
																if($rank==$row['rank']){echo " selected";}
																echo ">".$row['rank'];
																echo "</option>";
															}												
													}
												  
												  ?>

											</select>
										</td>
										<td>
											<select class="ddowns" name="status" id="status" onchange=''>
												  <option value="">Status</option>
												  <?php
													if(isset($statusdd))
													{
														while($row = $statusdd->fetch_assoc())
															{
																echo "<option value='".$row['status']."' ";
																if($status==$row['status']){echo " selected";}
																echo ">".$row['status'];
																echo "</option>";
															}												
													}
												  
												  ?>
											</select>
										</td>
										<td>
											<input class='test' type="text" name="service" id="service" onBlur='' 
											value='<?php if(!empty($_REQUEST['service'])){ echo $_REQUEST['service'];}?>' placeholder="Service">

										</td>
										<td>
											<input class='test' type="text" name="captain" id="captain" onBlur='' 
											value='<?php if(!empty($_REQUEST['captain'])){ echo $_REQUEST['captain'];}?>' placeholder="Captain">

										</td>
										<td>
											<input class='test' type="text" name="lieutenant" id="lieutenant" onBlur='' 
											value='<?php if(!empty($_REQUEST['lieutenant'])){ echo $_REQUEST['lieutenant'];}?>' placeholder="Lieutenant">

										</td>
										<td>
											<input class='test' type="text" name="commander" id="commander" onBlur='' 
											value='<?php if(!empty($_REQUEST['commander'])){ echo $_REQUEST['commander'];}?>' placeholder="Commander">

										</td>
										<td>
											<input class='test' type="text" name="yearmin" id="yearmin" onBlur='' 
											value='<?php if(!empty($_REQUEST['yearmin'])){ echo $_REQUEST['yearmin'];}?>' placeholder="YearMin">

										</td>
										<td>
											&nbsp;
										</td>
										<td>
											<input class='test' type="text" name="ref" id="ref" onBlur='' 
											value='<?php if(!empty($_REQUEST['ref'])){ echo $_REQUEST['ref'];}?>' placeholder="Reference">

										</td>
										</tr>
										
										
										<!-- row3  -->
<!--										<tr>										
										<th>
											Firstname
										</th>
										<th>
											First Name<br>Variants
										</th>										
										<th>
											&nbsp;
										</th>
										<th>
											&nbsp;
										</th>
										<th>
											&nbsp;
										</th>
										<td>
											&nbsp;
										</td>								
										<th>
											&nbsp;
										</th>
										<td>
											&nbsp;
										</td>								
										<td>
											Year To
										</td>								
										<td>
											&nbsp;
										</td>								
										<td>
											&nbsp;
										</td>								
										</tr>
-->
										<!--  row4 -->
										<tr>

										<td>
											<input class='test' type="text" name="fname" id="fname" onBlur='' 
											value='<?php if(!empty($_REQUEST['fname'])){ echo $_REQUEST['fname'];}?>' placeholder="Firstname">


										</td>
										<td>
											<font size='-2'>First name variations</font>
											<input class='test' type="checkbox" name="fname_var" id="fname_var" value='true' <?php echo $chkfname_var; ?> >

										</td>
										<td>
											&nbsp;
										</td>										
										<td>
											&nbsp;
										</td>										
										<td>
											&nbsp;
										</td>										
										<td>
											&nbsp;
										</td>										
										<td>
											&nbsp;
										</td>										
										<td>
											&nbsp;
										</td>										
										<td>
											<input class='test' type="text" name="yearmax" id="yearmax" onBlur='' 
											value='<?php if(!empty($_REQUEST['yearmax'])){ echo $_REQUEST['yearmax'];}?>' placeholder="Year to">

										</td>
										<td>
											&nbsp;
										</td>
										<td>
											&nbsp;
										</td>

										</tr>	
									</table>
									
								</div>		
								
								<!-- SEARCH PARAMETERS PANEL -->
								<div class="panel-body col-sm-9">

									<div class="col-sm-12">
										<div class="form-group">
											<div class="col-sm-2">										
												&nbsp; 
											</div>	
										</div>
										<div class="form-group">
											<div class="col-sm-3">										
												<button type="button" class="btn btn-default" name="clear" onclick="clearform()">Clear</button>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-3">										
												<button type="button" class="btn btn-default" name="searchsubmit" onclick="validatesearch()">Search</button>
											</div>
										</div>
										
										<div class="form-group">								
											<div class="col-sm-4">																				
												<button type="button" class="btn btn-default" name="downloadds" id="downloadds" onclick='download_ds()'>Download Results</button>
											</div>												
										</div>											
									</div>										
									 
									</div> <!-- close Search parameters panel -->

								</form><!-- close main search form -->

									
								<div class="container col-sm-12">
									<h2 class="searchheader"><b>Results table</b></h2>
									<?php  
										if(isset($mainsearch))
											{
												if($mainsearch->num_rows>0)
												{
													echo "<p>".$mainsearch->num_rows." records found</p>";
													echo "<table class='ptest2 table-bordered table-hover' width='50%'>
														<tr>
														<th><a href='#' onclick='setorderby(\"Person_Name\")'>Name</a></th>
														<th><a href='#' onclick='setorderby(\"Origin\")'>Origin</a></th>
														<th><a href='#' onclick='setorderby(\"Rank\")'>Rank</a></th>
														<th><a href='#' onclick='setorderby(\"Stat\")'>Status</a></th>
														<th><a href='#' onclick='setorderby(\"Service\")'>Service</a></th>
														<th>Captain</th>
														<th>Lieutenant</th>
														<th>Commander</th>
														<th><a href='#' onclick='setorderby(\"S_Date\")'>Service Date</a></th>
														<th><a href='#' onclick='setorderby(\"Full_Reference\")'>Reference</a></th>
														<th><a href='#' onclick='setorderby(\"Source_Type\")'>Source Type</a></th>
														</tr>";												
														// sid= soldier id
														while($row = $mainsearch->fetch_assoc()) {
																	echo "<tr><form name='gotorefform'  id='gotorefform".$row["ID"]."' action='biosearch.php' method='POST'>
																	<input type='hidden' name='sid".$row["ID"]."' id='sid".$row["ID"]."' value='".$row["ID"]."'>
																	<input type='hidden' name='pid' id='pid".$row["ID"]."' value='".$row["ID_Person"]."'>
																	<input type='hidden' name='datasets' id='datasets' value='".$datasets."'></form><td>";
																	if(!empty($row['ID_Person'])){ echo "<a href='".$_SERVER['PHP_SELF']."?id=".$row['ID_Person']."' onclick='gotoref(\"".$row["ID"]."\",\"".$row['ID_Person']."\")'>".$row['Person_Name']."</a>";}
																	else{echo $row["Person_Name"];}
																	echo "</td><td>"
																	.$row["Origin"]."</td><td>"
																	.$row["Rank"]."</td><td>"
																	.$row["Stat"]."</td><td>"
																	.$row["Service"]."</td><td>";
																	if(!empty($row['ID_Captain'])){ echo "<a href='".$_SERVER['PHP_SELF']."?id=".$row['ID_Captain']."' onclick='gotoref(\"".$row["ID"]."\",\"".$row['ID_Captain']."\")'>".$row['Captain']."</a>";}
																	else{echo $row["Captain"];}
																	echo "</td><td>";
																	if(!empty($row['ID_Lieutenant'])){ echo "<a href='".$_SERVER['PHP_SELF']."?id=".$row['ID_Lieutenant']."' onclick='gotoref(\"".$row["ID"]."\",\"".$row['ID_Lieutenant']."\")'>".$row['Lieutenant']."</a>";}
																	else{echo $row["Lieutenant"];}																	
																	echo "</td><td>";
																	if(!empty($row['ID_Commander'])){ echo "<a href='".$_SERVER['PHP_SELF']."?id=".$row['ID_Commander']."' onclick='gotoref(\"".$row["ID"]."\",\"".$row['ID_Commander']."\")'>".$row['Commander']."</a>";}
																	else{echo $row["Commander"];}																	
																	echo "</td><td>"				
																	.$row["S_Date"]."</td><td>";
																	if(!empty($row['Link_URL'])){ echo "<a href='".$row['Link_URL']."' >".$row['Full_Reference']."</a>";}
																	else{echo $row["Full_Reference"];}																	
																	echo "</td><td>"
																	.$row["Source_Type"]."</td></tr>"
																	;
																}
														echo "</table>";	
												}
												else{echo "<br>No results"; }
											}
											
										
									
									
									?>
								</div>								
								
							</div>						
						
						<!-- end smain search-->
										
						<!-- External LINKS-->		
						<div class="container">
							<h2 class="searchheader"><b>External links</b></h2>
							<?php
								if(isset($linkdata))
									{
										if($linkdata->num_rows>0)
										{
											echo "<ul>
												<a href='".$linkdata['Link_URL']."' >".$linkdata['Link_Caption']."</a>
												</ul>";										
										}
										else{echo "Empty link fields";}
									}
								else{echo "<br>No link data";}
							?>
							
						</div>					
                <!--//.island-->
            </div>
                <!--//.cell-->
            		
					</div>	
                         <!--<div class="cell-md-b50 rule rule-bottom"> -->
                        <h2 class="cell-t10 cell-b5 cell-md-t50 accordion__toggle" data-toggle="toggle" data-target="#associated-research-centres">
					
					
							<!--		COMMENT OUT PROJECT TEAM -RK
					
					
							Project Team
							<span class="glyphicon glyphicon-menu-down pull-right visible-xs visible-sm" aria-hidden="true"></span>
						</h2>
						<ul class="list-group-columns-md list-group-plain list-group-links
									rule rule-top rule-none-md
									cell-v10 cell-md-v0
									accordion__item" id="associated-research-centres">
							<li class="list-group-item"><b class="pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5" role="presentation"></b><a href="/research/research-centres-henley.aspx?CdsDrId=the-centre-for-institutional-performance">Person 1</a></li>
							<li class="list-group-item"><b class="pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5" role="presentation"></b><a href="http://www.reading.ac.uk/economic-history/">Person 2</a></li>
							<li class="list-group-item"><b class="pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5" role="presentation"></b><a href="http://www.reading.ac.uk/sustainability-in-the-built-environment/sbe-home.aspx">Person 3</a></li>
							<li class="list-group-item"><b class="pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5" role="presentation"></b><a href="http://www.reading.ac.uk/tsbe/tsbe-home-2.aspx">Person 4</a></li>
						</ul>
					-->
                </div>
                
					
				
				
            </div>
            <!--//.accordion-->
        </div>
        <!--//.col-->

        <!--//.col-->
    </div>
    <!--//.row-->
    <!-- ############## end intro content ############## -->
</div>
<!--//.contain -->
</main>

<!-- Bootstrap core JavaScript
================================================== -->

<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
  if (typeof jQuery == 'undefined')
  {
     document.write(unescape("%3Cscript src='/hwss/assets/js/jquery-1.11.2.min.js' type='text/javascript'%3E%3C/script%3E"));
  }
</script>
<script src="/js/henley-business-school-bootstrap.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/js/ie10-viewport-bug-workaround.js"></script>

<script src="/js/ksearch3.4.js"></script>
<!-- begin SnapEngage code - invisibile -->
<script type="text/javascript">
  (function() {
    var se = document.createElement('script'); se.type = 'text/javascript'; se.async = true; se.src = '//storage.googleapis.com/code.snapengage.com/js/52077d78-5f51-4ba7-a2a8-975c6c266d3b.js';
    var done = false;
    se.onload = se.onreadystatechange = function() {
      if (!done&&(!this.readyState||this.readyState==='loaded'||

this.readyState==='complete')) {
        done = true;
        /* Place your SnapEngage JS API code below */
        /* SnapEngage.allowChatSound(true); Example JS API: Enable 

sounds for Visitors. */
      }
    };
    var s = document.getElementsByTagName('script')[0]; 

s.parentNode.insertBefore(se, s);
  })();
</script>
<!-- end SnapEngage code -->
<script type="text/javascript">
 _bizo_data_partner_id = "7423";
</script>
<script type="text/javascript">
(function() {
 var s = document.getElementsByTagName("script")[0];
 var b = document.createElement("script");
 b.type = "text/javascript";
 b.async = true;
 b.src = (window.location.protocol === "https:" ? "https://sjs" : "http://js") + ".bizographics.com/insight.min.js";
 s.parentNode.insertBefore(b, s);
})();
</script>
<noscript>
 <img height="1" width="1" alt="" style="display:none;" src="//www.bizographics.com/collect/?pid=7423&fmt=gif" />
</noscript>


</body>
</html>
