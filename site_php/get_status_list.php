<?php
 
function get_status_list($conn=null)
	{
		if($conn)
			{			
				$queryall="
					SELECT 
						`r_Status`.`Presentation` AS `status`
					FROM
						`r_Status`
					WHERE
						(`r_Status`.`Is_Standard_Value` IS TRUE)
				"; 
				
				//echo $queryall;
					try
						{	
							$data=$conn->query($queryall);
							//$conn->close();
							if($data!=null)
								{	return $data;}
						}
						
						
					catch(Exception $e)
						{
							//echo $e;
							return false;
							//$conn->close();
						}
			}

		return false;	
	}
	

/*
//test
include "../dbopen.php";
$d=get_status_list($conn);

if($d)
	{
		if ($d->num_rows > 0) {
			echo "<table><tr><th>Name</th></tr>";
			// output data of each row
			while($row = $d->fetch_assoc()) {
				echo "<tr><td>".$row["status"]."</td></tr>";
			}
			echo "</table>";
		} else {
			echo "0 results";
		}
	}
else{echo "no results!---".$d;}
*/


?>