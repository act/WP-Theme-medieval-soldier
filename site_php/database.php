<?php
include "../dbopen.php";


$pdata=null;  // holds person data from simple search
$sdata=null; //holds full soldier data from longname search
$rdata=null; //holds full reference data from reference search
$simpledata=null;  // holds fname  surname id from simple search
$referencedata=null;  // holds id of reference data
$soldierdata=null;  //holds ids of soldier searched for
$fred="";

//foreach($_REQUEST as $key=>$value){echo "key=".$key."   value=".$value;}
//exit;


if(isset($_REQUEST['fname']) && isset($_REQUEST['surname']))
	{
		//
		include "namesearch.php";
		$fn=$_REQUEST['fname'];
		$sn=$_REQUEST['surname'];
		//echo "found request!";
		$simpledata=namesearch($fn,$sn);
	}

if(isset($_REQUEST['getperson']) )
	{
		include "getrecord.php";
		$id=trim($_REQUEST['getperson']);
		//echo "found request  for ".$id;
		$pdata=getrecord($id);
		
	}

	
if(isset($_REQUEST['soldiersearchtxt']))
	{

		include "longnamesearch.php";
		$ln=$_REQUEST['soldiersearchtxt'];
		//echo "found request!";
		$soldierdata=longnamesearch($ln);
	}

if(isset($_REQUEST['getsoldier']) )
	{
		include "getrecord.php";
		//include "getsoldier.php";
		$id=trim($_REQUEST['getsoldier']);
		$fred="found request for id=".$id;
		$sdata=getrecord($id);
	}

	
?>
<!DOCTYPE html>
<!--[if lte IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> 
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' /><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="Henley Business School">



<link rel="canonical" href="" />

<title>Medieval Soldier</title>

<!-- Bootstrap core CSS -->
<link href="/css/henley-business-school-bootstrap.css" rel="stylesheet">

<script src="/js/head.js"></script>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Favicons -->
<link rel="apple-touch-icon" href="/apple-touch-icon.png">
<link rel="icon" href="/favicon.ico">

<script>
function showNames(str) {
  var xhttp;
  if (str.length < 2) { 
    document.getElementById("txtHint").innerHTML = "";
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      document.getElementById("txtHint").innerHTML = xhttp.responseText;
    }
  };
  xhttp.open("GET", "getnames.php?q="+str, true);
  xhttp.send();   
}
function validatesimplesearch()
	{
		var fname=document.getElementById('fname').value;
		var sname=document.getElementById('surname').value;
		if(fname.length<1 && sname.length<1){return false;}
		//alert(sname);
		document.getElementById('frmnamesearch').submit();
		return true;
		
	}
	
function validatesoldiersearch()
	{
		var lname=document.getElementById('soldiersearchtxt').value;
		if(lname.length<1){return false;}
		//alert(lname);
		document.getElementById('frmsoldiersearch').submit();
		return true;
		
	}

	
function validatereferencesearch()
	{
		var lname=document.getElementById('fxxxxame').value;
		if(lname.length<1 && sname.length<1){return;}
		//alert(sname);
		document.getElementById('frmreferencesearch').submit();
		return true;
		
	}

	
function submitperson(pid)
	{
		f=document.getElementById(pid);
		//alert(pid);
		f.submit();
		return true;
		
	}

		
function submitsoldier(fid)
	{
		f=document.getElementById(fid);
		//alert(document.getElementById('getsoldier5').value);
		f.submit();
		return true;
		
	}
	
function gotomainsearch	()
	{
		document.location='maindbsearch.php';
	}
</script>



<script src="//use.typekit.net/alk7sin.js"></script>
<script>try{Typekit.load();}catch(e){}</script>

</head>
<body>
	<a id="skippy" class="sr-only sr-only-focusable" href="#content">
		<div class="container"><span class="skiplink-text">Skip to main content</span></div>
	</a>

	<div class="dropdown navbar-dropdown navbar-dropdown-menu">
			<div class="navbar-fixed navbar-fixed-top">
				<nav class="navbar-inverse navbar" style="background-color:black">
					<div class="container navbar-container">
						<div class="container col-sm-6" style="background-color:black;padding:0px">
							<div class="container col-sm-12" style="background-color:black;padding:0px" >
								<a class="navbar-brand navbar-brand-hbs" href="http://www.henley.ac.uk/">Henley Business School</a>

								<a class="navbar-brand navbar-brand-soton" href="http://www.southampton.ac.uk"> Soton</a>
							</div>
						</div>


<!--
						<ul class="nav navbar-nav" id="desktop-nav">
								<li>
									
									<a href="#subnav-desk-1" data-toggle-desktopnav data-target="#subnav-desk-1" data-toggle-group="main-nav">About</a>
									
								</li>

								
						</ul>	
					
						<div class="dropdown navbar-dropdown" style="background-color:#652511;">
								 Search toggle
							<button class="navbar-right navbar-toggle search-toggle" id="search-toggle" aria-label="Show search form" data-toggle-search data-target="#search-form" data-toggle-group="main-nav" >
								<b aria-hidden="true" class="glyphicon glyphicon-search"></b>
								<span class="icon-bar" aria-hidden="true"></span>
								<span class="icon-bar" aria-hidden="true"></span>
								<span class="icon-bar" aria-hidden="true"></span>
								<span class="navbar-toggle-close">&times;</span>
							</button>
							Search form
							<div class="search" id="search-form">
								<div class="container">
									<div class="row">
										<div class="col-xs-12 col-md-8 col-md-offset-4 col-lg-7 col-lg-offset-5">
											<form class="search-form" role="search" aria-labelledby="search-toggle" autocomplete="off" autocapitalize="off">
												<input type="text" name="query" id="searchQuery" class="form-control search-control query noSubmit" placeholder="Search Henley">
												<button type="submit" class="search-submit btn btn-default btn-primary" data-search-submit>
													<b aria-hidden="true" class="glyphicon glyphicon-search"></b>
													<span>Search</span>
												</button>
											</form>
										</div>
									</div>
									
									<div class="row">
										<div class="col-xs-12 search-wrapper">
											
											<div class="search-results" id="searchResults">
											</div>
										</div>
									</div>
								</div>
							</div> 
						</div>
-->							
						





							<!-- ########################################################### MOBILE NAVIGATION TOGGLE ########################################################### -->
						<div class="dropdown navbar-dropdown navbar-dropdown-menu">			                
							<button type="button" class="navbar-toggle" data-toggle-group="main-nav" id="navbar-toggle-mobile" data-target="#mobile-nav" data-toggle-mobilenav>
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar" aria-hidden="true"></span>
								<span class="icon-bar" aria-hidden="true"></span>
								<span class="icon-bar" aria-hidden="true"></span>
								<span class="navbar-toggle-close">&times;</span>
							</button>
							<!-- Mobile navigation -->
							<nav class="navbar-nav-mobile-wrapper island-gray-darker" id="mobile-nav" aria-labelledby="navbar-toggle">
								<ul class="nav nav-arrow-right navbar-nav navbar-nav-mobile nav-dark">
									<li class="dropdown">
										<!-- active -->
										<!-- TOP LEVEL MENU LINKS -->
										<a href="#" data-toggle="dropdown" data-target="#" id="subnav1-toggle">About</a>
										<a href="/database/maindbsearch.php">Database</a>

										
										<!-- <span class="sr-only">(current)</span> -->
										<ul class="nav nav-sub nav-light nav-arrow-right" id="subnav-1" aria-labelledby="subnav1-toggle">
											<li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1" data-toggle="dropdown" data-target="#subnav-1">Back</a></li>
											<li class="dropdown">
													<li><a href="/publications/ms_project.html" data-close-desktopnav>Project</a></li>
													<li><a href="/publications/publications.html" data-close-desktopnav>Publications</a></li>
													<li><a href="/publications/soldier_profiles.html" data-close-desktopnav>Soldier Profiles</a></li>
													<li><a href="/publications/agincourt_600.html" data-close-desktopnav>Agincourt 600</a></li>
													<li><a href="/publications/miscellanea.html" data-close-desktopnav>Miscellanea</a></li>												
												<!--
												<ul id="subnav-1-subnav-1" class="nav nav-sub nav-light nav-arrow-right">
													<li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1-subnav-1" data-toggle="dropdown" 
													data-target="#subnav-1-subnav-1">Back</a></li>
													
													<li><a href="/database/maindbsearch.php" data-close-mobilenav>Use help</a></li>
													<li><a href="/publications/help.html" data-close-mobilenav>Help</a></li>

												</ul>
												-->
											</li>
											<!--<li class="dropdown">
												<a href="#subnav-1-subnav-2" data-toggle="dropdown" data-target="#subnav-1-subnav-2">About</a>
												<ul id="subnav-1-subnav-2" class="nav nav-sub nav-light nav-arrow-right">
													<li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1-subnav-2" data-toggle="dropdown"
													data-target="#subnav-1-subnav-2">Back</a></li>
													<li><a href="/publications/ms_project.html" data-close-desktopnav>Project</a></li>
													<li><a href="/publications/publications.html" data-close-desktopnav>Publications</a></li>
													<li><a href="/publications/soldier_profiles.html" data-close-desktopnav>Soldier Profiles</a></li>
													<li><a href="/publications/agincourt_600.html" data-close-desktopnav>Agincourt 600</a></li>
													<li><a href="/publications/miscellanea.html" data-close-desktopnav>Miscellanea</a></li>
													
												</ul>
											</li>-->
											<!--
											<li class="dropdown">
												<a href="#subnav-1-subnav-3" data-toggle="dropdown" data-target="#subnav-1-subnav-3">Guidance Notes</a>
												<ul id="subnav-1-subnav-3" class="nav nav-sub nav-light nav-arrow-right">
													<li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1-subnav-3" data-toggle="dropdown" data-target="#subnav-1-subnav-3">Back</a></li>
													<li><a href="/guidance/normandygarrisons.html" data-close-mobilenav>Did your ancestor serve in the garrisons of Normandy 1415-1450?</a></li>
													<li><a href="/guidance/agincourt">Was your ancestor on the Agincourt campaign with Henry V?</a></li>
													
												</ul>
											</li>
											<li class="dropdown">
												<a href="#subnav-1-subnav-4" data-toggle="dropdown" data-target="#subnav-1-subnav-4">Soldier Profiles</a>
												<ul id="subnav-1-subnav-4" class="nav nav-sub nav-light nav-arrow-right">
													<li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1-subnav-4" data-toggle="dropdown" data-target="#subnav-1-subnav-4">Back</a></li>
													<li><a href="/profles/soldier-profiles.html" data-close-mobilenav>Profles</a></li>
													
												</ul>
											</li>
											<li class="dropdown">
												<a href="#subnav-1-subnav-5" data-toggle="dropdown" data-target="#subnav-1-subnav-5">Dissemination</a>
												<ul id="subnav-1-subnav-5" class="nav nav-sub nav-light nav-arrow-right">
													<li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1-subnav-5" data-toggle="dropdown" data-target="#subnav-1-subnav-5">Back</a></li>
													<li><a href="/dissemination/dissemination.html" data-close-mobilenav>Dissemination to Date</a></li>
													
													
												</ul>
											</li>
											<li class="dropdown">
												<a href="#subnav-1-subnav-6" data-toggle="dropdown" data-target="#subnav-1-subnav-6">Contributions</a>
												<ul id="subnav-1-subnav-6" class="nav nav-sub nav-light nav-arrow-right">
													<li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-1-subnav-6" data-toggle="dropdown" data-target="#subnav-1-subnav-6">Back</a></li>
													<li><a href="/contributions/contributions.html" data-close-mobilenav>Contributions</a></li>
												
												</ul>
											</li>
											-->
										</ul>
									</li>
									
									
									<li class="dropdown">
										<a href="#" data-toggle="dropdown" data-target="#" id="subnav2-toggle">Help</a>
										<ul class="nav nav-sub nav-light nav-arrow-right" id="subnav-2" aria-labelledby="subnav2-toggle">

											
													<li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-2-subnav-1" 
													data-toggle="dropdown" data-target="#subnav-2-subnav-1">Back</a></li>
													<li><a href="/publications/help.html" data-close-desktopnav>How to search</a></li>
													<li><a href="/publications/how_to_cite.html" data-close-desktopnav>How to cite</a></li>
													<li><a href="/publications/dataset_descriptions.html" data-close-desktopnav>Datasets described</a></li>
													<li><a href="/publications/source_info.html" data-close-desktopnav>Primary sources</a></li>
													<li><a href="/publications/research_guides.html" data-close-desktopnav>Search Guides</a></li>											
											<!--<li class="dropdown">
												<a href="#subnav-2-subnav-1" data-toggle="dropdown" data-target="#subnav-2-subnav-1">Help</a>
												<ul id="subnav-2-subnav-1" class="nav nav-sub nav-light nav-arrow-right">
													<li class="nav-item-dark nav-item-arrow-light nav-item-arrow-left"><a href="#subnav-2-subnav-1" 
													data-toggle="dropdown" data-target="#subnav-2-subnav-1">Back</a></li>
													<li><a href="/publications/help.html" data-close-desktopnav>How to search</a></li>
													<li><a href="/publications/how_to_cite.html" data-close-desktopnav>How to cite</a></li>
													<li><a href="/publications/dataset_descriptions.html" data-close-desktopnav>Datasets described</a></li>
													<li><a href="/publications/source_info.html" data-close-desktopnav>Primary sources</a></li>
													<li><a href="/publications/research_guides.html" data-close-desktopnav>Search Guides</a></li>
																			
												</ul>
											</li>-->
											
										</ul>
										<!--
										<li class="dropdown secondary">
											<a href="http://www.reading.ac.uk/">University of Reading</a>
											<a href="http://www.southampton.ac.uk/">University of Southampton</a>
										</li>
										-->
									</li>
									
									<!--         <li class="dropdown secondary">
											<a href="/about-us/online-resources-log-in/">Log in</a>
									</li> -->
								</ul>
								
							</nav>
						</div> 
						 <!-- END MOBILE NAVIGATION -->
						
						
						<!-- ########################################################### DESKTOP NAVIGATION ########################################################### -->
						<nav class="navbar-right navbar-nav-desktop" style="background-color:#000000;">
							<div class="cell-v5 clearfix">
								<div class="btn-group btn-group-bar pull-right text-muted" role="group">
									<a href="https://www.reading.ac.uk" class="btn btn-plain text-small ">University of Reading</a>
									<!-- <a href="#" class="btn btn-plain text-small ">Alumni</a> -->
									<!-- <a href="#" class="btn btn-plain text-small ">International</a> -->
									<a href="http://www.southampton.ac.uk" class="btn btn-plain text-small">University of Southampton</a>
									
									
							
									
								</div>
							</div>
							<ul class="nav navbar-nav" id="desktop-nav">
								<li>
									<!--  class="active" -->
									<a href="#subnav-desk-1" data-toggle-desktopnav data-target="#subnav-desk-1" data-toggle-group="main-nav">About</a>
									<!--span class="sr-only">(current)</span-->
								</li>
								<li>
									<a href="/database/maindbsearch.php">Database</a>
									<!--<a href="#subnav-desk-2" data-toggle-desktopnav data-target="#subnav-desk-2" data-toggle-group="main-nav">Database</a>-->
								</li>
								<li>
									<a href="#subnav-desk-2" data-toggle-desktopnav data-target="#subnav-desk-2" data-toggle-group="main-nav">Help</a>
								</li>							
							</ul>
						</nav>
					</div>
					<!-- /.container-fluid -->
				</nav>
				<!-- Desktop sub navigation -->
				<ul class="nav nav-sub nav-sub-desktop" id="subnav-desk-1">
					<!-- Subnav 1 -->
					<li class="dropdown">
						<a href="#subnav-desk-1-subnav-desk-1" data-toggle="toggle" data-toggle-group="subnav-desk-1" data-target="#subnav-desk-1-subnav-1" data-toggle-min="1" class="active">About</a>
						<ul id="subnav-desk-1-subnav-1" class="nav nav-sub open">
							<li><a href="/publications/ms_project.html" data-close-desktopnav>Project</a></li>
							<li><a href="/publications/publications.html" data-close-desktopnav>Publications</a></li>
							<li><a href="/publications/soldier_profiles.html" data-close-desktopnav>Soldier Profiles</a></li>
							<li><a href="/publications/agincourt_600.html" data-close-desktopnav>Agincourt 600</a></li>
							<li><a href="/publications/miscellanea.html" data-close-desktopnav>Miscellanea</a></li>
						</ul>
					</li>
					
					

						<!--
						<li class="dropdown">


						<a href="#subnav-desk-1-subnav-2" data-toggle="toggle" data-toggle-group="subnav-desk-1" data-target="#subnav-desk-1-subnav-2" data-toggle-min="1">afgadf</a>

						<ul id="subnav-desk-1-subnav-2" class="nav nav-sub">
							<li><a href="/database/maindbsearch.php" data-close-desktopnav>Use af</a></li>
							<li><a href="/publications/help.html" data-close-desktopnav>Hafffelp</a></li>
							
							
							
						</ul>
						
					</li>--<


						<!--
					<li class="dropdown">
						<a href="#subnav-desk-1-subnav-desk-1" data-toggle="toggle" data-toggle-group="subnav-desk-1" data-target="#subnav-desk-1-subnav-1" data-toggle-min="1" class="active">The Team</a>
						<ul id="subnav-desk-1-subnav-1" class="nav nav-sub open">
							<li><a href="/team/a-curry.html" data-close-desktopnav>Professor Anne Curry</a></li>
							<li><a href="/team/a-bell.html" data-close-desktopnav>Dr Adrian R Bell</a></li>
							<li><a href="/team/a-king.html" data-close-desktopnav>Dr Andy King</a></li>
							<li><a href="/team/d-simpkin.html" data-close-desktopnav>Dr David Simpkin</a></li>
							<li><a href="/team/a-chapman.html" data-close-desktopnav>Adam Chapman</a></li>
							<li><a href="/team/team-publications.html" data-close-desktopnav>Team Publications</a></li>
							<li><a href="/team/advisory-board.html" data-close-desktopnav>Advisory Board</a></li>
						</ul>
					</li>
                        --> 
                </ul>
					<!-- SUBNAV 2  -->
				<ul class="nav nav-sub nav-sub-desktop" id="subnav-desk-2">

					<li class="dropdown">
						<a href="#subnav-desk-2-subnav-desk-1" data-toggle="toggle" data-toggle-group="subnav-desk-2" data-target="#subnav-desk-2-subnav-1" class="active" data-toggle-min="1">Help</a>
						<ul id="subnav-desk-2-subnav-1" class="nav nav-sub open">

							<li><a href="/publications/help.html" data-close-desktopnav>How to search</a></li>
							<li><a href="/publications/how_to_cite.html" data-close-desktopnav>How to cite</a></li>
							<li><a href="/publications/dataset_descriptions.html" data-close-desktopnav>Datasets described</a></li>
							<li><a href="/publications/source_info.html" data-close-desktopnav>Primary sources</a></li>
							<li><a href="/publications/research_guides.html" data-close-desktopnav>Search Guides</a></li>							
						</ul>
					</li>
					<!--
					<li class="dropdown">
						<a href="#subnav-desk-2-subnav-2" data-toggle="toggle" data-toggle-group="subnav-desk-2" data-target="#subnav-desk-2-subnav-2" data-toggle-min="1">yyyyyyyyyy</a>
						<ul id="subnav-desk-2-subnav-2" class="nav nav-sub">
							<li><a href="/research" data-close-desktopnav>trwyty</a></li>
						</ul>
					</li>-->
				</ul>	
			</div>
				  
	</div><!-- /.container-fluid -->
				</nav>
						 <!-- END DESKTOP NAVIGATION -->
				
</div>
<main role="main" id="content">
    <!-- hero -->
<div class="cell-v30 jumbotron island island-gray-dark jumbotron-img" style="background-image: url('/images/medieval-soldiers.png')">
    <div class="container">
        <div class="row">
    <div class="col-xs-6">
        <h1 class="block-heading block-heading-l5 block-heading-b5 block-heading-md-l-reset cell-md-t20">
            <b>Medieval Soldier</b>
        </h1>
    </div><!--//.col-->
</div><!--//.row-->

    </div>
    <!--//.container-->
</div>
<!--//.island-->
<!-- include hbs/news/featured.html -->
<div class="island-outline outline-v">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-back">
                <!-- server side backlink, visible on mobile only-->
                <a href="#" aria-label="Go back up one level">
                    <b class="glyphicon glyphicon-menu-left" aria-hidden="true"></b>
                </a>
            </li>
            <li class="breadcrumb-path">
                <a href="/index.html">Home</a>
            </li>
			
			<!-- 
			Comment Out Breadcrums RK 
			
			<li class="breadcrumb-path">
                <a href="#">Database</a>
            </li>
            <li class="breadcrumb-parent">
                <a href="#">Sub-section</a>
				-->
            </li>
            <li class="active breadcrumb-current">
                <span>Current page</span>
            </li>
			
        </ol><!--//.breadcrumb-->
		</div><!--//.container-->
</div><!--//.island-outline-->

<div class="container cell-v50 cell-md-t60">
    <!-- ############## start intro content ############## -->
    <div class="row">
        <div class="col-xs-12 col-md-8">
            
            <!-- intro text -->
            <div class="cell-b20 cell-md-b0">
                <div class="island island-outline">
                    <div class="cell-20">
                        <h2 class="h3 block-heading block-heading-pull-t50 block-heading-l20"><b>Search the Database</b></h2>
						
						
						<!-- start soldier profile search -->
							<div class="container">
								<div class="panel panel-default">
									<div class="panel-body">
									  <h2 class="searchheader"><b>Search for a soldier profile</b></h2>
									  <form class="form-inline" role="form" id="frmsoldiersearch" name="frmsoldiersearch" method="POST" action="database.php">
										<div class="form-group">
											<div class="col-sm-8">
												<input type="text" class="form-control" id="soldiersearchtxt" name="soldiersearchtxt" placeholder="Enter text">
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-2">										
												<button type="button" class="btn btn-default" name="soldiersubmit" onclick="validatesoldiersearch()">Search</button>
											</div>
										</div>
										
										<div class="form-group">								
											<div class="col-sm-2">																				
												<button type="button" class="btn btn-default" name="soldierbrowse" id="soldierbrowse" onclick="gotomainsearch()" >Browse</button>
											</div>												
										</div>
									  </form>
									</div> <!-- close panel body -->	
								</div> <!-- panel close -->
							</div>

						<div class="container" id="soldiersearchresultsdiv">
						<!-- container for take results of above searches -->
							<?php
								//echo $fred;  //DEBUG
								if ($soldierdata) 
									{
										echo "<table class='table-condensed table-hover'><tr><th>Name</th></tr>";
										// output data of each row
										while($row = $soldierdata->fetch_assoc()) {
											echo "<tr><td>
											<form class='form-inline' role='form' id='frmgetsoldier".$row['ID']."' name='frmgetsoldier".$row['ID']."' method='POST' action='biosearch.php'>
											<input type='hidden' id='pid".$row['ID']."' name='pid' value='".$row['ID']."'>
											<a href='#' onclick='return submitsoldier(\"frmgetsoldier".$row['ID']."\")'>".$row["Longname"]."</a>
											</form>
											</td></tr>";
										}
										echo "</table>";
									} 
								
								
								elseif($sdata) 
									{
										if($sdata->num_rows>0)
											{
												echo "<table class='table-condensed table-hover'>";
										
												while($row = $sdata->fetch_assoc()) 
													{
														echo "<tr><th>ID</th><td>".$row["ID"]."</td></tr>
															<tr><th>Longname</th><td>".$row["Longname"]."</td><tr>
															<tr><th>Bio</th><td>".$row["Bio"]."</td><tr>
															<tr><th>Bio_Author</th><td>".$row["Bio_Author"]."</td><tr>
															<tr><th>Year_Birth</th><td>".$row["Year_Birth"]."</td><tr>
															<tr><th>Year_Death</th><td>".$row["Year_Death"]."</td><tr>
															<tr><th>Prefix</th><td>".$row["Prefix"]."</td><tr>
															<tr><th>Prenomen</th><td>".$row["Prenomen"]."</td><tr>
															<tr><th>Cognomen</th><td>".$row["Cognomen"]."</td><tr>
															<tr><th>Status</th><td>".$row["Status"]."</td><tr>
															<tr><th>dela</th><td>".$row["dela"]."</td><tr>
															<tr><th>Nomen</th><td>".$row["Nomen"]."</td><tr>
															<tr><th>Loconim</th><td>".$row["Loconim"]."</td><tr>
															<tr><th>Title</th><td>".$row["Title"]."</td><tr>
															";
													}
													echo "</table>";
										    }
										else{echo "No fields available for this record";}
										
									} 
								else {echo "0 results";}								
							?>
						</div><!-- End soldier search results div -->						
							
						<!-- end soldier profile search -->
						<div> &nbsp;</div>
						
						<!-- start simple search -->
							<div class="container" style="padding-top:10px">
								<div class="panel panel-default">
									<div class="panel-body">							
							
									  <h2 class="searchheader"><b>Simple Search</b></h2>
									  <form class="form-inline" role="form" id="frmnamesearch" method="POST" action="database.php">
									  
										<div class="form-group form-group-sm">									  
												<label class="control-label col-sm-2" for="fname">First name</label>
												<div class="col-sm-3">
												  <input type="text" class="form-control" 
												  id="fname" name="fname"
												  <?php  echo $_REQUEST['fname']; ?>
												  placeholder="Enter first name">
												</div>
										</div>	
										
										<div class="form-group  form-group-sm">											
											<label class="control-label col-sm-3" for="surname">Surname</label>
											<div class="col-sm-3">
												<input type="text" class="form-control" 
												id="surname" name="surname"
												  <?php  echo $_REQUEST['surname']; ?>
												placeholder="Enter surname">
											</div>			
										</div>	
										
										<div class="form-group">											
											<div class="col-sm-2">
												<button type="button" class="btn btn-default" name="soldiersubmit" onclick="validatesimplesearch()">Search</button>
											</div>
										</div>											
										
									  </form>
									</div> <!-- close panel body -->	
								</div> <!-- panel close -->									  
							</div>
						<!-- end simple search -->	
						
						<div class="container" id="simplesearchresultsdiv">
						<!-- container for take results of above searches -->
							<?php 
								if (isset($simpledata)) 
									{
										echo "<table class='table-condensed table-hover'><tr><th>Name</th></tr>";
										// output data of each row
										while($row = $simpledata->fetch_assoc()) {
											echo "<tr><td>
											<form class='form-inline' role='form' name='frmgetperson".$row['ID']."' id='frmgetperson".$row['ID']."' method='POST' action='maindbsearch.php'>
											<input type='hidden' id='pid".$row['ID']."' name='pid' value='".$row['ID']."'>
											<a href='#' onclick='return submitperson(\"frmgetperson".$row['ID']."\")'>".$row['First_Name']." ".$row['Surname']."</a>

											</form>
											</td></tr>";
										}
										echo "</table>";
									} 
								
								
								elseif(isset($pdata)) 
									{
										echo "<table class='table-condensed table-hover'>";
										while($row = $pdata->fetch_assoc()) 
										    {
												echo "<tr><th>ID</th><td>".$row["ID"]."</td></tr>
													<tr><th>Longname</th><td>".$row["Longname"]."</td><tr>
													<tr><th>Bio</th><td>".$row["Bio"]."</td><tr>
													<tr><th>Bio_Author</th><td>".$row["Bio_Author"]."</td><tr>
													<tr><th>Year_Birth</th><td>".$row["Year_Birth"]."</td><tr>
													<tr><th>Year_Death</th><td>".$row["Year_Death"]."</td><tr>
													<tr><th>Prefix</th><td>".$row["Prefix"]."</td><tr>
													<tr><th>Prenomen</th><td>".$row["Prenomen"]."</td><tr>
													<tr><th>Cognomen</th><td>".$row["Cognomen"]."</td><tr>
													<tr><th>Status</th><td>".$row["Status"]."</td><tr>
													<tr><th>dela</th><td>".$row["dela"]."</td><tr>
													<tr><th>Nomen</th><td>".$row["Nomen"]."</td><tr>
													<tr><th>Loconim</th><td>".$row["Loconim"]."</td><tr>
													<tr><th>Title</th><td>".$row["Title"]."</td><tr>
													";
											}
										echo "</table>";
									} 
								else {echo "0 results";}							
							?>
						</div><!-- End simple search div -->						


						<div> &nbsp;  </div>
						
						<!-- start reference search
							<div class="container" style="padding-top:10px">
								<div class="panel panel-default">
									<div class="panel-body">							
							
									  <h2 class="searchheader"><b>Reference search</b></h2>
									  <form class="form-inline" role="form" id="frmreferencesearch" method="POST" action="searchreference.php">
										<div class="form-group">
											<div class="col-sm-8">
												<input type="text" class="form-control" id="referencesearchtxt" name="referencesearchtxt" placeholder="Enter text">
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-2">											
												<button type="button" class="btn btn-default" name="referencesubmit" onclick="">Search</button>
											</div>
										</div>
										
										<div class="form-group">								
											<div class="col-sm-2">										
												<button type="button" class="btn btn-default" name="referencebrowse" id="referencebrowse" >Browse</button>
											</div>
										</div>
									  </form>
									</div> 	
								</div> 
									  </div>
						end reference search -->
						
						
						
						
						
						
                    <!--//.cell-->
                </div><!--END SEARCH db DIV -->
				<div class="container" id="referencesearchresultsdiv">
				<!-- container for take results of above searches -->
					<?php
						if(isset($referencedata))
							{
								if ($referencedata->num_rows > 0) 
								{
									echo "<table class='table-condensed'><tr><th>Name</th></tr>";
									// output data of each row
									while($row = $referencedata->fetch_assoc()) {
										echo "<tr><td><a href=''>".$row["First_Name"]." ".$row["Surname"]."</a></td></tr>";
									}
									echo "</table>";
								} 
							}
							
							else {echo "0 results";}
							
					?>
				</div>
				
				
                <!--//.island-->
            </div>
            <!--//.cell-->
            		
					</div>	
                <!--<div class="cell-md-b50 rule rule-bottom"> -->
                    <h2 class="cell-t10 cell-b5 cell-md-t50 accordion__toggle" data-toggle="toggle" data-target="#associated-research-centres">
					
					
					<!--		COMMENT OUT PROJECT TEAM -RK
					
					
                        Project Team
                        <span class="glyphicon glyphicon-menu-down pull-right visible-xs visible-sm" aria-hidden="true"></span>
                    </h2>
                    <ul class="list-group-columns-md list-group-plain list-group-links
                                rule rule-top rule-none-md
                                cell-v10 cell-md-v0
                                accordion__item" id="associated-research-centres">
                        <li class="list-group-item"><b class="pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5" role="presentation"></b><a href="/research/research-centres-henley.aspx?CdsDrId=the-centre-for-institutional-performance">Person 1</a></li>
                        <li class="list-group-item"><b class="pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5" role="presentation"></b><a href="http://www.reading.ac.uk/economic-history/">Person 2</a></li>
                        <li class="list-group-item"><b class="pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5" role="presentation"></b><a href="http://www.reading.ac.uk/sustainability-in-the-built-environment/sbe-home.aspx">Person 3</a></li>
                        <li class="list-group-item"><b class="pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5" role="presentation"></b><a href="http://www.reading.ac.uk/tsbe/tsbe-home-2.aspx">Person 4</a></li>
                    </ul>
					-->
                </div>
                
					
				
				
            </div>
            <!--//.accordion-->
        </div>
        <!--//.col-->

        <!--//.col-->
    </div>
    <!--//.row-->
    <!-- ############## end intro content ############## -->
</div>
<!--//.contain -->
</main>

<!-- Bootstrap core JavaScript
================================================== -->

<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
  if (typeof jQuery == 'undefined')
  {
     document.write(unescape("%3Cscript src='/hwss/assets/js/jquery-1.11.2.min.js' type='text/javascript'%3E%3C/script%3E"));
  }
</script>
<script src="/js/henley-business-school-bootstrap.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/js/ie10-viewport-bug-workaround.js"></script>

<script src="/js/ksearch3.4.js"></script>
<!-- begin SnapEngage code - invisibile -->
<script type="text/javascript">
  (function() {
    var se = document.createElement('script'); se.type = 'text/javascript'; se.async = true; se.src = '//storage.googleapis.com/code.snapengage.com/js/52077d78-5f51-4ba7-a2a8-975c6c266d3b.js';
    var done = false;
    se.onload = se.onreadystatechange = function() {
      if (!done&&(!this.readyState||this.readyState==='loaded'||

this.readyState==='complete')) {
        done = true;
        /* Place your SnapEngage JS API code below */
        /* SnapEngage.allowChatSound(true); Example JS API: Enable 

sounds for Visitors. */
      }
    };
    var s = document.getElementsByTagName('script')[0]; 

s.parentNode.insertBefore(se, s);
  })();
</script>
<!-- end SnapEngage code -->
<script type="text/javascript">
 _bizo_data_partner_id = "7423";
</script>
<script type="text/javascript">
(function() {
 var s = document.getElementsByTagName("script")[0];
 var b = document.createElement("script");
 b.type = "text/javascript";
 b.async = true;
 b.src = (window.location.protocol === "https:" ? "https://sjs" : "http://js") + ".bizographics.com/insight.min.js";
 s.parentNode.insertBefore(b, s);
})();
</script>
<noscript>
 <img height="1" width="1" alt="" style="display:none;" src="//www.bizographics.com/collect/?pid=7423&fmt=gif" />
</noscript>


</body>
</html>
