</main>

<div id="tooltip">
	tooltip content
</div>

<?php wp_footer(); ?>
<!-- ================================================== 
           Bootstrap core JavaScript
======================================================= -->

<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
  if (typeof jQuery == 'undefined')
  {
     document.write(unescape("%3Cscript src='/hwss/assets/js/jquery-1.11.2.min.js' type='text/javascript'%3E%3C/script%3E"));
  }
</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/henley-business-school-bootstrap.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo get_template_directory_uri(); ?>/js/ie10-viewport-bug-workaround.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/ksearch3.4.js"></script>
<!-- begin SnapEngage code - invisibile -->
<script type="text/javascript">
  (function() {
    var se = document.createElement('script'); se.type = 'text/javascript'; se.async = true; se.src = '//storage.googleapis.com/code.snapengage.com/js/52077d78-5f51-4ba7-a2a8-975c6c266d3b.js';
    var done = false;
    se.onload = se.onreadystatechange = function() {
      if (!done&&(!this.readyState||this.readyState==='loaded'||

this.readyState==='complete')) {
        done = true;
        /* Place your SnapEngage JS API code below */
        /* SnapEngage.allowChatSound(true); Example JS API: Enable 

sounds for Visitors. */
      }
    };
    var s = document.getElementsByTagName('script')[0]; 

s.parentNode.insertBefore(se, s);
  })();
</script>
<!-- end SnapEngage code -->
<script type="text/javascript">
 _bizo_data_partner_id = "7423";
</script>
<script type="text/javascript">
(function() {
 var s = document.getElementsByTagName("script")[0];
 var b = document.createElement("script");
 b.type = "text/javascript";
 b.async = true;
 b.src = (window.location.protocol === "https:" ? "https://sjs" : "http://js") + ".bizographics.com/insight.min.js";
 s.parentNode.insertBefore(b, s);
})();
</script>

<script type="text/javascript">
$(document).ready(function(){
   $('p:empty').remove(); //Rmove empty <p> tags
});
</script>

<noscript>
 <img height="1" width="1" alt="" style="display:none;" src="//www.bizographics.com/collect/?pid=7423&fmt=gif" />
</noscript>
</body>
</html>