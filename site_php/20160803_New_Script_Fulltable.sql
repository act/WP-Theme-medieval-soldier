

Set
	@Datasets = '11111111', 
	@ID_Person = Null, 
	@ID_Source = Null, 
	@SName = Null, 
	@FName = Null,  
	@FName_Var = True, 
	@Origin = Null, 
	@Rank = Null, 
	@Stat = Null,		 
	@Service = Null, 
	@Captain = Null, 
	@Lieutenant = Null, 
	@Commander = Null, 		 
	@YearMin = Null, 
	@YearMax = Null, 
	@Ref = Null, 
		/*@orderby="Person_Name", 
		@asc="ASC", */
	@empty = '-nil-' ;



SELECT 
	`v_FullTable`.`ID`,
    `v_FullTable`.`Person_Name`,
    `v_FullTable`.`ID_Person`,
    `v_FullTable`.`Origin`,
    `v_FullTable`.`Rank`,
    `v_FullTable`.`Stat`,
    `v_FullTable`.`Service`,
    
	`v_FullTable`.`Captain`,
    `v_FullTable`.`ID_Captain`,
    
	`v_FullTable`.`Lieutenant`,

    `v_FullTable`.`ID_Lieutenant`,
    
    `v_FullTable`.`Commander` 	,

    `v_FullTable`.`ID_Commander`,
    
    `v_FullTable`.`S_Date`,
    `v_FullTable`.`ID_Source`,
    `v_FullTable`.`Source_Type`,
    `v_FullTable`.`Full_Reference`,
    `v_FullTable`.`Link_URL`
    
FROM `v_FullTable`
	


WHERE
	(Substring(@Datasets, t_ID_Dataset, 1) <> '0') 
		
    AND IF (@ID_Person is Null, True, ID_Person = @ID_Person) 
	AND IF (@ID_Source is Null, True, ID_Source = @ID_Source)  
       
	AND IF (@SName is Null, True, IF(t_Surname is Null, @SName = @empty, t_Surname like @SName)) 
	AND ( 
		(IF (@FName is Null, True, IF(t_First_Name is Null, @FName = @empty, t_First_Name like @FName))) 
                
		OR (  
			(@FName_Var = True) AND (NOT(@FName is Null)) 
			AND 
            (
				t_ID_Basic_Firstname in (
					Select distinct ID_Basic 
					from r_First_Name 
					WHERE ((Name_Value like @FName) and (not isnull(ID_Basic)))
				)
			) 
		) 
	
	) 
	
	AND IF (@Rank is Null, True, IF(Rank is Null, @Rank = @empty, Rank like @Rank)) 
	AND IF (@Stat is Null, True, IF(Stat is Null, @Stat = @empty, Stat like @Stat)) 
					 
	AND IF (@YearMin is Null, True, LEFT(IFNULL(t_Date_in_Service, IFNULL(t_Service_Year,IFNULL(t_Issue_Date,''))),4)>=@YearMin) 
	AND IF (@YearMax is Null, True, LEFT(IFNULL(t_Date_in_Service,	IFNULL(t_Service_Year,IFNULL(t_Issue_Date,''))),4)<=@YearMax) 
					 
	AND IF (@Captain is Null, True, IF(Captain is Null, @Captain = @empty, Captain like @Captain)) 
	AND IF (@Lieutenant is Null, True, IF(Lieutenant is Null, @Lieutenant = @empty, Lieutenant like @Lieutenant)) 
	AND IF (@Commander is Null, True, IF(Commander is Null, @Commander = @empty, Commander like @Commander)) 
	    
	AND IF (@Origin is Null, True, IF(Origin is Null, @Origin = @empty, Origin like @Origin)) 
    AND IF (@Service is Null, True, IF (Service is Null, @Service = @empty, Service like @Service)) 
	AND IF (@Ref is Null, True, IF(Full_Reference is Null, @Ref = @empty, Full_Reference like @Ref))            	
		
GROUP BY 
	`ID` , 
    `Person_Name` , 
    `ID_Person` , 
    `Origin` , 
    `Rank` , 
    `Stat` , 
    `Service` , 
    `S_Date` , 
    `ID_Source` , 
    `Source_Type` , 
    `Full_Reference` , 
    `Link_URL`