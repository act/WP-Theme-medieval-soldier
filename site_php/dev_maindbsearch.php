<?php

include "../dbopen.php";
include "get_datasets.php";
include "get_main_search.php";
include "get_status_list.php";
include "get_rank_list.php";
include "getperson.php";


$dsetsdata=get_datasets($conn);  //get fields
$dsetrows=$dsetsdata->num_rows;
$sel_ds=[];  // chkbx defaults

$dsetsstr=str_pad("",$dsetrows,1);  //set default dataset to length of rows
for($i=0;$i<$dsetrows;$i++){array_push($sel_ds,"checked");}  //set chkbx defaults

$datasets=$dsetsstr; //set default dataset for hidden var

$id_person="Null";
$id_source="Null";
$sname="Null";
$fname="Null"; 
$fname_var="Null";
$origin="Null";
$rank="Null";
$stat="Null";		
$service="Null";
$captain="Null";
$lieutenant="Null";
$commander="Null"; 		
$yearmin="Null";
$yearmax="Null";
$ref="Null";
$orderby="S_Date";
$asc="ASC";
		

if(isset($_REQUEST['datasets']))
	{
		// set checkbox data for datasets panel
		$dsetsstr=$_REQUEST['datasets'];
		$datasets=$_REQUEST['datasets'];
		$split_ds=str_split($dsetsstr);
		$dslen=count($split_ds);
		for($i=0;$i<$dslen;$i++)
			{
				if($split_ds[$i]==1){$sel_ds[$i]="checked";}
				else{$sel_ds[$i]="";}
			}
	}    
$statusdd=get_status_list($conn);
$rankdd=get_rank_list($conn);
if(isset($_REQUEST))
	{
		if(!empty($_REQUEST['surname'])){$surname="'".$_REQUEST['surname']."'";}else{$surname="Null";}
		if(!empty($_REQUEST['fname'])){$fname="'".$_REQUEST['fname']."'";}else{$fname="Null";}
		if(!empty($_REQUEST['fname_var'])&& $_REQUEST['fname_var']=='true'){$chkfname_var='checked';$fname_var="true";}else{$chkfname_var='';$fname_var="false";}
		if(!empty($_REQUEST['origin'])){$origin="'".$_REQUEST['origin']."'";}else{$origin="Null";}
		if(!empty($_REQUEST['rank'])){$rank="'".$_REQUEST['rank']."'";}else{$rank="Null";}
		if(!empty($_REQUEST['status'])){$status="'".$_REQUEST['status']."'";}else{$status="Null";}
		if(!empty($_REQUEST['service'])){$service="'".$_REQUEST['service']."'";}else{$service="Null";}
		if(!empty($_REQUEST['captain'])){$captain="'".$_REQUEST['captain']."'";}else{$captain="Null";}
		if(!empty($_REQUEST['lieutenant'])){$lieutenant="'".$_REQUEST['lieutenant']."'";}else{$lieutenant="Null";}
		if(!empty($_REQUEST['commander'])){$commander="'".$_REQUEST['commander']."'";}else{$commander="Null";}
		if(!empty($_REQUEST['yearmax'])){$yearmax="'".$_REQUEST['yearmax']."'";}else{$yearmax="Null";}
		if(!empty($_REQUEST['yearmin'])){$yearmin="'".$_REQUEST['yearmin']."'";}else{$yearmin="Null";}
		if(!empty($_REQUEST['ref'])){$ref="'".$_REQUEST['ref']."'";}else{$ref="Null";}
		if(!empty($_REQUEST['orderby'])){$orderby=$_REQUEST['orderby'];}else{$orderby="S_Date";}
		if(!empty($_REQUEST['asc'])){$asc=$_REQUEST['asc'];}else{$asc="ASC";}
	}

$empty = "'-nil-'";

//foreach($_REQUEST as $key=>$value){echo $_REQUEST[$key]."=".$value;} 
/*
		$Datasets,
		$ID_Person,
		$ID_Source,
		$SName,
		$FName, 
		$FName_Var,
		$Origin,
		$Rank,
		$Stat,		
		$Service,
		$Captain,
		$Lieutenant,
		$Commander, 		
		$YearMin,
		$YearMax,
		$Ref,
		$empty
*/
if(isset($_REQUEST['mainsearch']))
	{
		foreach($_REQUEST as $key=>$value){$_REQUEST[$key]=mysqli_real_escape_string($conn,$value);}
		
		$mainsearch=get_main_search
		(
			$conn,
			"'".$_REQUEST['datasets']."'",
			"Null",
			"Null",
			$surname,
			$fname,
			$fname_var,
			$origin,
			$rank,
			$status,		
			$service,
			$captain,
			$lieutenant,
			$commander, 		
			$yearmin,
			$yearmax,
			$ref,
			$orderby,
			$asc,
			$empty
			);
	}
else
	{
		$mainsearch=false;   //get_main_search($conn);
	}

	
if(isset($_REQUEST['pid']))
	{
		// get data for bio panel
		$pid=$_REQUEST['pid'];
		$record=getperson($pid)->fetch_assoc();
		$mainsearch=get_main_search
		(
			$conn,
			$dsetsstr,
			"Null",
			"Null",
			"'".$record['Surname']."'",
			"'".$record['First_Name']."'"			
			);
	} 
	
	
if(isset($_REQUEST['downloadds']))
	{
		if($_REQUEST['downloadds']==1)
			{
				$_REQUEST['downloadds']=0;
				// create csv for download
				if($mainsearch)
					{
						$fstr="downloads/download_ds.csv";
						$f=fopen($fstr,'w');
						if($f)
							{
								$line="\"Name\",\"Origin\",\"Rank\",\"Status\",\"Service\",\"Captain\",\"Lieutenant\",";
								$line.="\"Commander\",\"Service Date\",\"Source Type\",\"Reference\"\r\n";
								fwrite($f,$line);
								
								while($row=$mainsearch->fetch_assoc())
									{
										$line="\"".$row["Person_Name"]."\",\"".$row["Origin"]."\",\"".$row["Rank"]."\",\""
										.$row["Stat"]."\",\"".$row["Service"]."\",\"".$row['Captain']."\",\"".$row['Lieutenant']."\",\""
										.$row['Commander']."\",\"".$row["S_Date"]."\",\"".$row["Source_Type"]."\",\"".$row["Full_Reference"]."\"\r\n";
										fwrite($f,$line);
									}
									$line=" , , , , , , ,";
									$line.=" , , , \r\n";
									fwrite($f,$line);
								fclose($f);

								if (file_exists($fstr)) {
									header('Content-Description: File Transfer');
									header('Content-Type: application/octet-stream');
									header('Content-Disposition: attachment; filename="'.basename($fstr).'"');
									header('Expires: 0');
									header('Cache-Control: must-revalidate');
									header('Pragma: public');
									header('Content-Length: ' . filesize($fstr));
									readfile($fstr);
								}
								
								unlink($fstr);
							}
					}
					
					
			}  		
		}		
		
?>
<!DOCTYPE html>
<!--[if lte IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> 
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' /><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="Henley Business School">



<link rel="canonical" href="" />

<title>Medieval Soldier</title>

<!-- Bootstrap core CSS -->
<link href="../css/henley-business-school-bootstrap.css" rel="stylesheet">

<script src="../js/head.js"></script>

<script src="../js/util_funcs.js"></script>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Favicons -->
<link rel="apple-touch-icon" href="/apple-touch-icon.png">
<link rel="icon" href="/favicon.ico">
<style>
#tooltip {
	font-size:0.7em;
	visibility:hidden;
	position:absolute;	    
    align-content: center;
    -webkit-align-content: center;
	border-radius: 25px;
    border: 2px solid #333399;
	background-color:#eeeeee;
	width: 250px;
    height: 85px;
	padding-top:8px;
	padding-left:18px;
    /*

	display: -webkit-flex;
    display: flex;*/
}

.test
	{
		width:100%;
	}
.ptest
{
	width:100%;
}
.ptest2
{
	width:100%;
}
.ddowns 
	{
		color:black;
	}
	
.smallfont{font-size:8; font-color:red;}
</style>


<script>
function showNames(str) {
  var xhttp;
  if (str.length < 2) { 
    document.getElementById("txtHint").innerHTML = "";
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      document.getElementById("txtHint").innerHTML = xhttp.responseText;
    }
  };
  xhttp.open("GET", "getnames.php?q="+str, true);
  xhttp.send();   
}



function validatesearch()
	{
		// block last download signal
		document.getElementById('downloadds').value=0;
		
		var fname=document.getElementById('fname').value;
		document.getElementById('fname').value=filterStr(fname);
	//	alert(document.getElementById('fname').value);
		
		var sname=document.getElementById('surname').value;
		document.getElementById('surname').value=filterStr(sname);
		
		//var fname_var=document.getElementById('fname_var').value;
		
		var origin=document.getElementById('origin').value;
		document.getElementById('origin').value=filterStr(origin);		
		
		var captain=document.getElementById('captain').value;	
		document.getElementById('captain').value=filterStr(captain);	
		
		var lieutenant=document.getElementById('lieutenant').value;
		document.getElementById('lieutenant').value=filterStr(lieutenant);
		
		var commander=document.getElementById('commander').value;
		document.getElementById('commander').value=filterStr(commander);		
		
		var service=document.getElementById('service').value;
		document.getElementById('service').value=filterStr(service);
		
		var yearmin=document.getElementById('yearmin').value;
		
		var yearmax=document.getElementById('yearmax').value;
		
		var ref=document.getElementById('ref').value;	
		document.getElementById('ref').value=filterStr(ref);
		
		if(!(yearmax<2050 && yearmax>500 || isInteger(yearmax) || yearmax==""))
			{
				alert("Year to must be a number or empty!");
				document.getElementById('yearmax').value="";
				return false;
			}
		if(!(yearmin<2050 && yearmin>500 || isInteger(yearmin) || yearmin==""))
			{
				alert("Year from must be a number or empty!");
				document.getElementById('yearmin').value="";
				return false;
			}

		document.getElementById('mainsearchform').submit();
		return true;
		
	}

function isInteger(n)
{
    var result=true;
    if(n==""){result=false;}
    if(n.indexOf('.')>-1){result=false;}
    if(isNaN(n)==true){result=false;}	
    // alert("result="+result);
	return result;
}

function filterStr(str)
	{
		if(str.length<1){return '';}
		var str1=str.replace(/['"<>/]/g,'_');
		var str2=str1.replace(/\*/g,'%');
		var str3=str2.replace(/\?/g,'_');	
		//alert('str=' +str+'  str1=' +str1+'  2=' +str2+'  3=' +str3);
		return str3;	
	}
	
function setDatasets()
	{
		var dsets=document.getElementById('datasets');  //get dataset string
		var lends=dsets.value.length;
		var newdsstr='';
		//loop over checkboxes
		for(var i=1;i<lends+1;i++)
			{
				var cbxid='dataset'+String(i);
				//alert('cbxid='+cbxid);
				var cbx=document.getElementById(cbxid);
				// build new dsets string
				if(cbx.checked==true){newdsstr=newdsstr+'1';}
				else{newdsstr=newdsstr+'0';}
			}
		dsets.value=newdsstr;
		//alert(dsets.value);
		
	}

function setorderby(field)	
	{
		var orderby=document.getElementById('orderby');
		var asc=document.getElementById('asc');
		orderby.value=field;
		if(asc.value=='ASC'){asc.value='DESC';}
		else{asc.value='ASC';}
		//alert(orderby.value + ' ' + asc);
		var mainfrm=document.getElementById('mainsearchform');
		mainfrm.submit();
		return true;
	}
	
function clearform()
	{
		var fname=document.getElementById('fname');
		//alert(fname.value);
		fname.value='';
		document.getElementById('surname').value='';
		document.getElementById('fname_var').checked=false;
		document.getElementById('rank').selectedIndex=0;
		document.getElementById('status').selectedIndex=0;
		
		document.getElementById('origin').value='';
		document.getElementById('captain').value='';		
		document.getElementById('lieutenant').value='';	
		document.getElementById('commander').value='';	
		document.getElementById('service').value='';	
		document.getElementById('yearmin').value='';
		document.getElementById('yearmax').value='';		
		document.getElementById('ref').value='';
	}	

function gotoref(fid,sid)	
	{
		var formid='gotorefform'+fid;
		var pidelname='pid'+fid;
		//alert(formid);
		//alert(pidelname);
		var f=document.getElementById(formid);
		var pidel=document.getElementById(pidelname);
		pidel.value=sid;
		//alert(f.id);
		f.submit();
		return true;
	}	

function download_ds()
	{
		var mainfrm=document.getElementById('mainsearchform');
		var dl=document.getElementById('downloadds');
		dl.value=1;
		//alert(dl.value);
		mainfrm.submit();
		dl.value=0;
		
	}
</script>



<script src="//use.typekit.net/alk7sin.js"></script>
<script>try{Typekit.load();}catch(e){}</script>

</head>
<body>


	<div class="container-fluid" style="padding-top: 50px;">

		<!-- ############## start intro content ############## -->
		<div class="container col-sm-12">
			<div class="col-xs-12 col-md-8">

			  
				<!-- intro text -->
				<div class="cell-b20 cell-md-b0">
					<div class="island island-outline" style="background-color:#ffffff">
						<div class="cell-20" style="background-color:#ffffff;padding-top: 1px;padding-left: 1px">
							<h2 class="h3 block-heading block-heading-pull-t50 block-heading-l20"><b>View Records</b></h2>
							
							
							<!-- start smain search-->
							<form class="form-group" name="mainsearchform" id="mainsearchform" action="maindbsearch.php" method="POST">
							
								<input type='hidden' name='mainsearch' id='mainsearch' value='mainsearch'>
								<input type='hidden' name='id_person' id='id_person' value='id_person'>
								<input type='hidden' name='id_source' id='id_source' value='id_source'>
								<input type='hidden' name='empty' id='empty' value='empty'>	
								<input type='hidden' name='downloadds' id='downloadds' value='0'>	
								
								<input type='hidden' name='orderby' id='orderby' value='<?php if(!empty($_REQUEST['orderby']))
									{ echo $_REQUEST['orderby'];}
								else{echo "S_Date";}
								?>'>	
								<input type='hidden' name='asc' id='asc' value='<?php if(!empty($_REQUEST['asc']))
									{ echo $_REQUEST['asc'];}
								else{echo "ASC";}
								?>'>						
								
								<div class="container-fluid" style="background-color:#ffffff"><!-- main search area div  -->
								

									
										<!--  DATASETS PANEL  -->
										<div class="container-fluid" style="background-color:#ffffff">
										  <h2 class="searchheader"><b>Datasets</b></h2>
											<?php
												//  fill DATASETS with data
												if($dsetsdata)
												{
													echo "<table class='table-condensed table-hover'><tr>";
													$i=0;
													while($row = $dsetsdata->fetch_assoc()) 
														{	
															$i++;
															echo "<td>

																  <input type='checkbox' name='dataset".$i."' id='dataset".$i.
																  "' value='".$row["ID"]."' onchange='setDatasets()' 														  
																  ".$sel_ds[$i-1]." 
                                                                  onMouseOver=\"showhint(this.id,'".$row["Hover"]."')\" 
															      onMouseOut=\"tooltip_hide('tooltip')\"><label>
																  &nbsp;".$row["Dataset"]."</label>
																										
															</td>
															";
														}
													echo "</tr></table>";										
												}
												else{echo "No Datasets!";}
											?>
											<input type="hidden" name="datasets" id="datasets" value='<?php echo $datasets;?>'>
										</div> <!-- close Datasets panel  -->
										
										 <!-- DIV HOLDING SEARCH AND BUTTON CELLS 1 & 2-->	
										<div class="container-fluid col-sm-12" style="background-color:#ffffff">
											
											 <!-- DIV HOLDING LHS CELL ITEMS -->	
											<div class="container col-sm-11" style="background-color:#ffffff;padding-left:1px">
														<h2 class="searchheader"><b>Search</b></h2>										
												 <!-- DIV HOLDING SURNAME AND FIRSTNAME -->	
												<div class="container col-sm-12" style="background-color:#ffffff">
																				
													<div class="container col-sm-4">	
															<input class='test' type="text" name="surname" id="surname" 
															value='<?php if(!empty($_REQUEST['surname']))	{ echo $_REQUEST['surname'];}?>' placeholder="Surname" 
															onMouseOver="showhint(this.id,'Enter full or fuzzy name (e.g. Sm*th or Sm%th)')" 
															onMouseOut="tooltip_hide('tooltip')">
													</div>
													<div class="container col-sm-4">	
															<input class='test' type="text" name="fname" id="fname" onBlur='' 
															value='<?php if(!empty($_REQUEST['fname'])){ echo $_REQUEST['fname'];}?>' placeholder="First name"
															onMouseOver="showhint(this.id,'Enter full or fuzzy name (e.g. Ed*d or Ed%d for Edward, Edmund)')" 
															onMouseOut="tooltip_hide('tooltip')">
													</div>
													<div class="container col-sm-4">
														<div class="container col-sm-12">
															<div class="container col-sm-11" style="font-size:100%;text-align:left">														
																<table style="border:0px solid black;">
																<tr>
																	<td style="width:90%">
																	First name variations</td>
																	<td style="width:10%">
																	<input class='test' type="checkbox" name="fname_var" id="fname_var" value='true' <?php echo $chkfname_var; ?> 
																	onMouseOver="showhint(this.id,'Check to receive related first names e.g. Steven or Etienne while searching for Stephen')" 
																	onMouseOut="tooltip_hide('tooltip')">
																	</td>
																</tr>
																</table>
															</div>
																																							
															<div class="container col-sm-1" style="text-align:left">
																&nbsp;
															</div>
														
														</div>
														
													</div>									

												</div> <!-- fn/sn close -->								

												<!-- div holding search options and buttons -->
												<div class="container col-sm-12" style="background-color:#ffffff;padding-top:20px;padding-bottom:20px:padding-left: 0px">
												
														
														<h3 class="searchheader"><b>Advanced</b></h3>
														<table class="ptest table-bordered table-hover">

															<tr>

															<td>
																<input class='test' type="text" name="origin" id="origin" onBlur='' 
																value='<?php if(!empty($_REQUEST['origin'])){ echo $_REQUEST['origin'];}?>' placeholder="Origin" 
																	onMouseOver="showhint(this.id,'Enter place, county (e.g. Bed* for Bedfordshire or Beds), nationality (e.g. %Gasc%)')" 
																	onMouseOut="tooltip_hide('tooltip')">
															</td>
															<td>
																<select class="ddowns" name="status" id="status" onchange=''>
																	  <option value="">Status</option>
																	  <?php
																		if(isset($statusdd))
																		{
																			while($row = $statusdd->fetch_assoc())
																				{
																					echo "<option value='".$row['status']."' ";
																					if(!empty($_REQUEST['status']))
																						{
																							if($_REQUEST['status']==$row['status']){echo " selected";}
																						}
																					echo ">".$row['status'];
																					echo "</option>";
																				}												
																		}
																	  
																	  ?>
																</select>
															</td>

															<td>
																
																<select class="ddowns" name="rank" id="rank" onchange=''>
																	  <option value="">Rank</option>
																	  <?php
																		if(isset($rankdd))
																		{
																			while($row = $rankdd->fetch_assoc())
																				{
																					echo "<option value='".$row['rank']."' ";
																					if(!empty($_REQUEST['rank']))
																						{
																							if($_REQUEST['rank']==$row['rank']){echo " selected";}
																						}
																					echo ">".$row['rank'];
																					echo "</option>";
																				}												
																		}
																	  
																	  ?>

																</select>
															</td>

															<td>
																<input class='test' type="text" name="service" id="service" 
																value='<?php if(!empty($_REQUEST['service'])){echo $_REQUEST['service'];}?>' placeholder="Service" 
																	onMouseOver="showhint(this.id,'Enter fuzzy location (e.g. %Caen% for all mentions of Caen) or type of service (e.g. %gar%)')" 
																	onMouseOut="tooltip_hide('tooltip')">
															</td>
															<td>
																<input class='test' type="text" name="captain" id="captain" onBlur='' 
																value='<?php if(!empty($_REQUEST['captain'])){ echo $_REQUEST['captain'];}?>' placeholder="Captain" 
																	onMouseOver="showhint(this.id,'Enter fuzzy name / highest title (e.g. %talbot%shrewsbury% )Order is e.g. Montagu, Thomas, earl of Salisbury (%salisbury%)')" 
																	onMouseOut="tooltip_hide('tooltip')">															
															</td>
															<td>
																<input class='test' type="text" name="lieutenant" id="lieutenant" onBlur='' 
																value='<?php if(!empty($_REQUEST['lieutenant'])){ echo $_REQUEST['lieutenant'];}?>' placeholder="Lieutenant"
 																	onMouseOver="showhint(this.id,'Enter fuzzy name (e.g. %fastolf%john% )Order is e.g. Fastolf, John, Sir (highest status always given)')" 
																	onMouseOut="tooltip_hide('tooltip')">																
															</td>
															<td>
																<input class='test' type="text" name="commander" id="commander" onBlur='' 
																value='<?php if(!empty($_REQUEST['commander'])){ echo $_REQUEST['commander'];}?>' placeholder="Commander" 
																	onMouseOver="showhint(this.id,'Enter fuzzy name / highest title (e.g. %talbot%shrewsbury% )Order is e.g. Montagu, Thomas, earl of Salisbury (%salisbury%)')" 
																	onMouseOut="tooltip_hide('tooltip')">
															</td>
															<td>
																<input class='test' type="text" name="yearmin" id="yearmin" onBlur='' 
																value='<?php if(!empty($_REQUEST['yearmin'])){ echo $_REQUEST['yearmin'];}?>' placeholder="Year from" 
																	onMouseOver="showhint(this.id,'Enter year, e.g. 1415. To search for a particular year put same date in Year from and Year to. Results will be given as YYYYMMDD.')" 
																	onMouseOut="tooltip_hide('tooltip')">
															</td>
															<td>
																<input class='test' type="text" name="yearmax" id="yearmax" onBlur='' 
																value='<?php if(!empty($_REQUEST['yearmax'])){ echo $_REQUEST['yearmax'];}?>' placeholder="Year to" 
																	onMouseOver="showhint(this.id,'Enter year, e.g. 1415. To search for a particular year put same date in Year from and Year to. Results will be given as YYYYMMDD.')" 
																	onMouseOut="tooltip_hide('tooltip')">
															</td>
															<td>
																<input class='test' type="text" name="ref" id="ref" onBlur='' 
																value='<?php if(!empty($_REQUEST['ref'])){ echo $_REQUEST['ref'];}?>' placeholder="Ref" 
																	onMouseOver="showhint(this.id,'Enter archival reference ')" 
																	onMouseOut="tooltip_hide('tooltip')">
															</td>
															</tr>

														</table>
														
													</div>
												
											</div>  <!-- CLose cell 1 div -->	
											
											<!-- CELL 2 DIV -->
											<div class="panel-body col-sm-1"  style="padding-top: 10px;padding-left: 1px;padding-right: 1px">	
											
												<!-- SEARCH BUTTONS PANEL 
												<div class="container col-sm-12" style="background-color:#ffee00">-->

													<div class="col-sm-12">

														<div class="form-group">
															<div class="col-sm-12">										
																<button type="button" class="btn btn-default" name="searchsubmit" onclick="validatesearch()">Search</button>
																<br><br>
															</div>
														</div>
														<div class="form-group">
															<div class="col-sm-12">										
																<button type="button" class="btn btn-default" name="clear" onclick="clearform()">Clear</button>
																<br><br>
															</div>
															
														</div>
														
														<div class="form-group">								
															<div class="col-sm-12">																				
																<button type="button" class="btn btn-default" name="dbdownload" id="dbdownload" onclick='download_ds()'>Download</button>
															</div>												
														</div>											
													</div>										
												 
												<!--</div>  close Search parameters panel -->
												
											</div>  <!-- CLose cell 2 div -->	
										</div>  <!-- CLose 2 cell div -->
								</form><!-- close main search form -->

									<!--      Results table     -->
									<div class="container-fluid col-sm-12" style="background-color:#ffffff;padding-top:50px;padding-left: 15px">
										<h2 class="searchheader"><b>Results table</b></h2>
											<?php  
												if(isset($mainsearch)&& !empty($mainsearch))
													{
														if($mainsearch->num_rows>0)
														{
																echo "<p>".$mainsearch->num_rows." records found</p>";
																echo "<div class='table-responsive'>";
																echo "<table class='table table-bordered table-hover''>
																<tr>
																<th><a href='#' onclick='setorderby(\"Person_Name\")'>Name</a></th>
																<th><a href='#' onclick='setorderby(\"Origin\")'>Origin</a></th>
																<th><a href='#' onclick='setorderby(\"Stat\")'>Status</a></th>
																<th><a href='#' onclick='setorderby(\"Rank\")'>Rank</a></th>
																<th><a href='#' onclick='setorderby(\"Service\")'>Service</a></th>
																<th><a href='#' onclick='setorderby(\"Captain\")'>Captain</a></th>
																<th><a href='#' onclick='setorderby(\"Lieutenant\")'>Lieutenant / Sub-Captain</a></th>
																<th><a href='#' onclick='setorderby(\"Commander\")'>Commander</a></th>
																<th><a href='#' onclick='setorderby(\"S_Date\")'>Service Date</a></th>
																<th><a href='#' onclick='setorderby(\"Source_Type\")'>Source Type</a></th>
																<th><a href='#' onclick='setorderby(\"Full_Reference\")'>Reference</a></th>
																														</tr>";
														
																//RESULTS
																while($row = $mainsearch->fetch_assoc()) {
																			echo "<tr style='font-size:0.66em;'><td>";
																			if(!empty($row['ID_Person'])){ echo "<a href='biosearch.php?id=".$row['ID_Person']."' onclick=''>".$row['Person_Name']."</a>";}
																			else{echo $row["Person_Name"];}
																			echo "</td><td>"
																			.$row["Origin"]."</td><td>"
																			.$row["Stat"]."</td><td>"
																			.$row["Rank"]."</td><td>"
																			.$row["Service"]."</td><td>";
																			echo $row["Captain"];
																			echo "</td><td>";
																			echo $row["Lieutenant"];																	
																			echo "</td><td>";
																			echo $row["Commander"];
																			echo "</td><td>"				
																			.$row["S_Date"]."</td><td>"
																			.$row["Source_Type"]."</td><td>";
																			if(!empty($row['Link_URL'])){ echo "<a href='".$row['Link_URL']."' >".$row['Full_Reference']."</a>";}
																			else{echo $row["Full_Reference"];}																	
																			echo "</td></tr>"
																			;
																		}
																echo "</table>";
															echo "</div>";
														}
														else{echo "<br>No records"; }
													}					
											?>
										</div>								
									
								</div>						

							
							<!-- end smain search-->				
					
					<!--//.island-->
				</div>
				<!--//.cell-->
						
						</div>	
					<!--<div class="cell-md-b50 rule rule-bottom"> -->
						<h2 class="cell-t10 cell-b5 cell-md-t50 accordion__toggle" data-toggle="toggle" data-target="#associated-research-centres">
						
						
						<!--		COMMENT OUT PROJECT TEAM -RK
						
						
							Project Team
							<span class="glyphicon glyphicon-menu-down pull-right visible-xs visible-sm" aria-hidden="true"></span>
						</h2>
						<ul class="list-group-columns-md list-group-plain list-group-links
									rule rule-top rule-none-md
									cell-v10 cell-md-v0
									accordion__item" id="associated-research-centres">
							<li class="list-group-item"><b class="pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5" role="presentation"></b><a href="/research/research-centres-henley.aspx?CdsDrId=the-centre-for-institutional-performance">Person 1</a></li>
							<li class="list-group-item"><b class="pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5" role="presentation"></b><a href="http://www.reading.ac.uk/economic-history/">Person 2</a></li>
							<li class="list-group-item"><b class="pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5" role="presentation"></b><a href="http://www.reading.ac.uk/sustainability-in-the-built-environment/sbe-home.aspx">Person 3</a></li>
							<li class="list-group-item"><b class="pull-left list-group-sub-icon glyphicon glyphicon-menu-right cell-t5" role="presentation"></b><a href="http://www.reading.ac.uk/tsbe/tsbe-home-2.aspx">Person 4</a></li>
						</ul>
						
					</div>-->
					
						
					
					
				</div>
				<!--//.accordion-->
			</div>
			<!--//.col-->

			<!--//.col-->
		</div>
		<!--//.row-->
		<!-- ############## end intro content ############## -->
	</div>
<!--//.contain -->
</main>

<div id="tooltip"">
	hhhhhhhhhhh
</div>




</body>
</html>
