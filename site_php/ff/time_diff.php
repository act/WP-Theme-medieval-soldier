<?php

function time_diff($dt1,$dt2,$units='ms')
	{
		try
		{
			$t1=strtotime($dt1);
			$t2=strtotime($dt2);
		}
		catch(Exception $e)
			{
				echo "Invalid time string...",$e->error();
				return false;
			}
		
		$tdiff=abs($t1-$t2);
		
		switch($units)
			{
				case 'Y'||'y' :$result=$tdiff/(1000*60*60*24/365);break;
				case 'M' :$result=$tdiff/(1000*60*60*24*(365/12));break;
				case 'd' :$result=$tdiff/(1000*60*60*24);break;
				case 'h' :$result=$tdiff/(1000*60*60);break;
				case 'm' :$result=$tdiff/(1000*60);break;
				case 's' :$result=$tdiff/1000;break;			
				case 'ms' :$result=$tdiff;break;				
				default :$result=$tdiff;break;
			}
		return $result;
	}

	
//test
$ts1="3-4-2016 16.57";
$ts2="13-6-2017 26.47"; // 10days,2months,1year,10mins,10s
echo "tdiff years=".time_diff($ts1,$ts2,'Y');
echo "tdiff months=".time_diff($ts1,$ts2,'M');
echo "tdiff days=".time_diff($ts1,$ts2,'d');
echo "tdiff hours=".time_diff($ts1,$ts2,'h');
echo "tdiff minutes=".time_diff($ts1,$ts2,'m');
echo "tdiff seconds=".time_diff($ts1,$ts2,'s');
echo "tdiff milli secs=".time_diff($ts1,$ts2,'ms');
echo "tdiff default?=".time_diff($ts1,$ts2,'xxx');
echo "tdiff error=".time_diff($ts1,"shit",'Y');









?>