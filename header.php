<?php
/**
 * The header for our theme.
 *
 * @package Medieval Soldier
 */

?>
<!DOCTYPE html>
<!--[if lte IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> 
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' /><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="Henley Business School">

<?php wp_head(); ?>


<link rel="canonical" href="" />

<title><?php echo get_bloginfo('name');?></title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/medievalsoldiers.css" media="screen">

<script src="<?php echo get_template_directory_uri(); ?>/js/head.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/util_funcs.js"></script>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Favicons -->
<link rel="apple-touch-icon" href="/apple-touch-icon.png">
<link rel="icon" href="/favicon.ico">



<script>
function showNames(str) {
  var xhttp;
  if (str.length < 2) { 
    document.getElementById("txtHint").innerHTML = "";
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      document.getElementById("txtHint").innerHTML = xhttp.responseText;
    }
  };
  xhttp.open("GET", "getnames.php?q="+str, true);
  xhttp.send();   
}



function validatesearch()
	{
		// block last download signal
		document.getElementById('downloadds').value=0;
		
		var fname=document.getElementById('fname').value;
		document.getElementById('fname').value=filterStr(fname);
	//	alert(document.getElementById('fname').value);
		
		var sname=document.getElementById('surname').value;
		document.getElementById('surname').value=filterStr(sname);
		
		//var fname_var=document.getElementById('fname_var').value;
		
		var origin=document.getElementById('origin').value;
		document.getElementById('origin').value=filterStr(origin);		
		
		var captain=document.getElementById('captain').value;	
		document.getElementById('captain').value=filterStr(captain);	
		
		var lieutenant=document.getElementById('lieutenant').value;
		document.getElementById('lieutenant').value=filterStr(lieutenant);
		
		var commander=document.getElementById('commander').value;
		document.getElementById('commander').value=filterStr(commander);		
		
		var service=document.getElementById('service').value;
		document.getElementById('service').value=filterStr(service);
		
		var yearmin=document.getElementById('yearmin').value;
		
		var yearmax=document.getElementById('yearmax').value;
		
		var ref=document.getElementById('ref').value;	
		document.getElementById('ref').value=filterStr(ref);
		
		if(!(yearmax<2050 && yearmax>500 || isInteger(yearmax) || yearmax==""))
			{
				alert("Year to must be a number or empty!");
				document.getElementById('yearmax').value="";
				return false;
			}
		if(!(yearmin<2050 && yearmin>500 || isInteger(yearmin) || yearmin==""))
			{
				alert("Year from must be a number or empty!");
				document.getElementById('yearmin').value="";
				return false;
			}

		document.getElementById('mainsearchform').submit();
		return true;
		
	}

function isInteger(n)
{
    var result=true;
    if(n==""){result=false;}
    if(n.indexOf('.')>-1){result=false;}
    if(isNaN(n)==true){result=false;}	
    // alert("result="+result);
	return result;
}

function filterStr(str)
	{
		if(str.length<1){return '';}
		var str1=str.replace(/['"<>/]/g,'_');
		var str2=str1.replace(/\*/g,'%');
		var str3=str2.replace(/\?/g,'_');	
		//alert('str=' +str+'  str1=' +str1+'  2=' +str2+'  3=' +str3);
		return str3;	
	}
	
function setDatasets()
	{
		var dsets=document.getElementById('datasets');  //get dataset string
		var lends=dsets.value.length;
		var newdsstr='';
		//loop over checkboxes
		for(var i=1;i<lends+1;i++)
			{
				var cbxid='dataset'+String(i);
				//alert('cbxid='+cbxid);
				var cbx=document.getElementById(cbxid);
				// build new dsets string
				if(cbx.checked==true){newdsstr=newdsstr+'1';}
				else{newdsstr=newdsstr+'0';}
			}
		dsets.value=newdsstr;
		//alert(dsets.value);
		
	}

function setorderby(field)	
	{
		var orderby=document.getElementById('orderby');
		var asc=document.getElementById('asc');
		orderby.value=field;
		if(asc.value=='ASC'){asc.value='DESC';}
		else{asc.value='ASC';}
		//alert(orderby.value + ' ' + asc);
		var mainfrm=document.getElementById('mainsearchform');
		mainfrm.submit();
		return true;
	}
	
function clearform()
	{
		var fname=document.getElementById('fname');
		//alert(fname.value);
		fname.value='';
		document.getElementById('surname').value='';
		document.getElementById('fname_var').checked=false;
		document.getElementById('rank').selectedIndex=0;
		document.getElementById('status').selectedIndex=0;
		
		document.getElementById('origin').value='';
		document.getElementById('captain').value='';		
		document.getElementById('lieutenant').value='';	
		document.getElementById('commander').value='';	
		document.getElementById('service').value='';	
		document.getElementById('yearmin').value='';
		document.getElementById('yearmax').value='';		
		document.getElementById('ref').value='';
	}	

function gotoref(fid,sid)	
	{
		var formid='gotorefform'+fid;
		var pidelname='pid'+fid;
		//alert(formid);
		//alert(pidelname);
		var f=document.getElementById(formid);
		var pidel=document.getElementById(pidelname);
		pidel.value=sid;
		//alert(f.id);
		f.submit();
		return true;
	}	

function download_ds()
	{
		var mainfrm=document.getElementById('mainsearchform');
		var dl=document.getElementById('downloadds');
		dl.value=1;
		//alert(dl.value);
		mainfrm.submit();
		dl.value=0;
		
	}
</script>


<script src="<?php echo get_template_directory_uri(); ?>/js/alk7sin.js"></script>
<script>try{Typekit.load();}catch(e){}</script> 

</head>