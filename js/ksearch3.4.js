/* 
from apapche
VERSION Date        WHO         Description
1.0.0   ??          Kirk Whelan Initial Release
1.1.0               Kirk Whelan Search moved to one directory down located at search
1.2.0   7/9/2013    Kirk Whelan added cursor track inside search container, then when outside close search
1.3.0   7/9/2013    André Mansi Tried to get the templating into place, and stop the wretched fadeout.
*/
var keyDownLongEventTime = 500;
var keyDownShortEventTime = 50;
var $search = null;
var $results = null;
var searchData = '';
var summarySoFar = '';
var keydownTimer = null;
var searchXY = [-1, -1, -1, -1];
$(document).ready(function () {
    var kSearchClearTimer;
    function clearSearchTimer() {
        if (kSearchClearTimer)
            clearInterval(kSearchClearTimer);
        kSearchClearTimer = setInterval(function () {
            $('#searchResults').fadeOut(3000);
        }, 5000);
    }
    $('#kSearchButton').bind('click', function () {
        clearKSearch();
    });
    $('#search-toggle').bind('click', function () {
        $results.fadeOut(1000, function () {
                clearKSearch();
            });
        clearKSearch();
    });
    $search = $('#searchQuery');
    $results = $('#searchResults');
    $('#searchResults').mousemove(function (event) {
        if (searchXY[0] == -1) {
            searchXY[0] = event.pageX;
            searchXY[1] = event.pageX;
            searchXY[2] = event.pageY;
            searchXY[3] = event.pageY;
        }
        else {
            searchXY[1] = searchXY[0];
            searchXY[0] = event.pageX;
            searchXY[3] = searchXY[2];
            searchXY[2] = event.pageY;
            //$('#debugInfo').html('' + (searchXY[1] - searchXY[0]) + ',' + (searchXY[3] - searchXY[2]));
        }
    });
    $('#searchResults').mouseleave(function () {
        var diffX = searchXY[1] - searchXY[0];
        var diffY = searchXY[3] - searchXY[2];
        if (searchXY[0] < this.offsetLeft + diffX + 5 || searchXY[0] > this.offsetLeft + this.offsetWidth + diffX - 5
                    || searchXY[2] < this.offsetTop + diffY + 5 || searchXY[2] > this.offsetTop + this.offsetHeight + diffY - 5) {   // slow fade
            // $results.fadeOut(3000, function () {
            //     clearKSearch();
            // });
        }
        //clearSearchTimer();
    });
    $search.bind('keydown', function () {
        if (keydownTimer != null) {
            clearTimeout(keydownTimer);
            keydownTimer = null;
        };
        // set keydown timer based on search length already in the the submit element
        var eventTime = $search.val().length > 1 ? keyDownLongEventTime : keyDownShortEventTime;
        keydownTimer = setTimeout(function () {
            var sText = $search.val();
            if (sText.length > 0 && searchChanged(sText)) {
            // alert("sText is greater than 0 and has been changed. Entered text is: " . val(sText));
                $(function () {
                    showGetResult(sText, null);
                });
            }
            else {
            // alert("sText is not greater than 0 or has not been changed.");
                if (sText.length == 0) {
                    $results.css({ 'display': 'none' });
                }
                //==$('#debugInfo').empty();
            }
        }, eventTime);
    })
            .bind('focus', function (e) {
                var $query = $(e.currentTarget);
                $query.parent().addClass('active');
            });
    function clearKSearch() {
        $search.val('');
        $results.css({ 'display': 'none' });
        $('#searchQuery').blur();
    }
    function searchChanged(searchText) {
        if (searchText != searchData) {
            searchData = searchText;
            //if (searchData == '')
            //$('#debugInfo').empty();
            return true;
        }
        return false;
    }
    function showGetResult(search, summary) {
        //if (summary != null)
        //    alert("summary:" + summary+","+$search.val());
        //$('#debugInfo').append('Search.aspx?s=' + search + '&p=' + summary + '<br />');
        //$('#debugInfo').append('Search.aspx?s=' + search + '&p=' + summary + '<br />');
        var groupId = 0;
        var group = $('#searchGroup');
        if (group != null)
            groupId = group.val();
        if (search.length > 1 && search[search.length - 1] == ' ')
            search = search.substring(0, search.length - 1) + '~';
        $.ajax({
            url: '/search.aspx',
            data: { s: search, g: groupId },
            success: function (data, status) {
                if (status == 'success') {
                    var jResultSet = $.parseJSON(data);
                    //$('#debugInfo').append(data);
                    if (jResultSet != null) {
                        var resTable = '<ul>';
                        var jResults = jResultSet.searchResults;
                        var jSummary = jResultSet.searchSummary; // not set yet
                        var jRlen = jResults.length;
                        for (jRi = 0; jRi < jRlen; jRi++) {
                            //set up the result group
                            var resSet = '<li><h2>';
                            var jSet = jResults[jRi];
                            resSet += jSet.category;
                            resSet += '</h2><ul>';
                            // then add results here
                            var urlList = jSet.results;
                            for (uis = 0; uis < urlList.length; uis++) {
                                var urlSet = urlList[uis];
                                resSet += '<li><a href="' + urlSet.url + '" title="' + urlSet.title + '">' + urlSet.title + '</a></li>'
                            }
                            //finish off the group
                            resSet += '</ul></li>';
                            resTable += resSet;
                        }
                        resTable += '<li class="cell-10">Having trouble finding something? Try searching the <a href="http://www.reading.ac.uk/search/public/search" target="_blank">University of Reading</a>.</li>';
                        resTable += '</ul>';
                        // resTable += '</div>';
                        if($results==null){
                            alert("no results area");
                        }
                        //alert("done "+resTable.length);
                        $results.html(resTable);
                        $results.css({ 'display': 'block' });
                        // If load on server gets high split the search requests
                        // Removed for now
                        /*
                        if (summary == null) {
                        var jRequest = jResultSet.searchRequest;
                        if ($search.val() == jRequest) {
                        //alert("extra");
                        showGetResult(search, jSummary);
                        }
                        }*/
                    }
                }
            },
            complete: function (xhr, statusText) {
                if (statusText != 'success')
                    $('#debugInfo').html(xhr.status + " " + statusText);
            },
            cache: false
        });
    };
    // function resultsArrived(data, status) {
    //    $('#searchResults').text("" + status + ":" + data);
    // }
});