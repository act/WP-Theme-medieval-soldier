<?php

//This theme has one menu location, on the left
add_action('init', 'add_menu');
function add_menu()
{
    register_nav_menu( 'primary', __( 'Primary Menu', 'UniversityOfReading' ) );
}


function get_menu_by_location( $location ) {
    if( empty($location) ) return false;

    $locations = get_nav_menu_locations();
    if( ! isset( $locations[$location] ) ) return false;

    $menu_obj = get_term( $locations[$location], 'nav_menu' );

    return $menu_obj;
}

// function to read the menu items and sort them in an array
function wp_get_menu_array($current_menu) {
 
    $array_menu = wp_get_nav_menu_items($current_menu);
    $menu = array();
    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {
            $menu[$m->ID] = array();
            $menu[$m->ID]['ID']      =   $m->ID;
            $menu[$m->ID]['title']       =   $m->title;
            $menu[$m->ID]['url']         =   $m->url;
            $menu[$m->ID]['children']    =   array();
        }
    }
    $submenu = array();
    foreach ($array_menu as $m) {
        if ($m->menu_item_parent) {
            $submenu[$m->ID] = array();
            $submenu[$m->ID]['ID']       =   $m->ID;
            $submenu[$m->ID]['title']    =   $m->title;
            $submenu[$m->ID]['url']  =   $m->url;
            $menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
        }
    }
    return $menu;
}

wp_register_script( 'dbscript', get_template_directory_uri() . '/js/db_script.js', array ( 'jquery' ), 1.1, true);
wp_register_script( 'headjs', get_template_directory_uri() . '/js/head.js', array ( 'jquery' ), 1.1, false);
wp_register_script( 'util_funcs', get_template_directory_uri() . '/js/util_funcs.js', array ( 'jquery' ), 1.1, true);



function remove_the_wpautop_function() {
    remove_filter( 'the_content', 'wpautop' );
    remove_filter( 'the_excerpt', 'wpautop' );
}
//add_action( 'after_setup_theme', 'remove_the_wpautop_function' ); //Removes all <p> tags. Too strong, commented on 03/07/2017...

/*
get_template_part("/site_php/dbopen.php");
get_template_part("/site_php/maindbsearch.php");
get_template_part("/site_php/get_datasets.php");
get_template_part("/site_php/get_rank_list.php");
get_template_part("/site_php/get_status_list.php");
get_template_part("/site_php/getperson.php");
*/
//get_template_part("/site_php/get_person_links.php");

//get_template_part("/site_php/getrecord.php");
//get_template_part("/site_php/getreference.php");
//get_template_part("/site_php/longnamesearch.php");
//
//get_template_part("/site_php/namesearch.php");
//get_template_part("/site_php/namesearch_pdo.php");

?>